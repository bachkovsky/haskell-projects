module Main where

import Data.Char
import System.Environment
import System.IO

shift :: (Int -> Int -> Int) -> Char -> Char -> Char
shift op offset ch = numToChar $ charToNum ch `op` charToNum offset
  where
    charToNum ch = ord ch - ord 'A'
    numToChar n = chr $ (n `mod` 26) + ord 'A'

vigenere :: String -> String -> String
vigenere secret = zipWith (shift (+)) (cycle secret) . concat . words

unvigenere :: String -> String -> String
unvigenere secret = zipWith (shift (-)) (cycle secret) . concat . words

main :: IO ()
main = do
  args <- getArgs
  case args of
    [secret, "-e"] -> go (vigenere secret)
    [secret, "-d"] -> go (unvigenere secret)
    other ->
      error
        ("Invalid arguments: " ++ show other ++ ". Use <secret> and [-e|-d]")
  where
    go f = (f <$> getContents) >>= putStr
