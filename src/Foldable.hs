module Foldable
  ( Tree
  ) where

import Data.Monoid

data Triple a =
  Tr a
     a
     a
  deriving (Eq, Show)

instance Foldable Triple where
  foldr f ini (Tr a b c) = foldr f ini [a, b, c]

data Tree a
  = Nil
  | Branch (Tree a)
           a
           (Tree a)
  deriving (Eq, Show)

testTree :: Tree Integer
testTree =
  Branch
    (Branch (Branch Nil 1 Nil) 2 (Branch Nil 3 Nil))
    4
    (Branch (Branch Nil 6 Nil) 5 (Branch Nil 7 Nil))

instance Foldable Tree where
  foldr _ ini Nil = ini
  foldr f ini (Branch l x r) = foldr f (f x (foldr f ini r)) l

newtype Preorder a =
  PreO (Tree a)
  deriving (Eq, Show)

newtype Postorder a =
  PostO (Tree a)
  deriving (Eq, Show)

newtype Levelorder a =
  LevelO (Tree a)
  deriving (Eq, Show)

instance Foldable Preorder where
  foldr _ ini (PreO Nil) = ini
  foldr f ini (PreO (Branch l x r)) =
    f x (foldr f (foldr f ini (PreO r)) (PreO l))

instance Foldable Postorder where
  foldr _ ini (PostO Nil) = ini
  foldr f ini (PostO (Branch l x r)) =
    foldr f (foldr f (f x ini) (PostO r)) (PostO l)

instance Foldable Levelorder where
  foldr f ini (LevelO tr) = foldr f ini (tbf [tr])
    where
      tbf [] = []
      tbf [Nil] = []
      tbf xs = map val xs ++ tbf (concatMap level xs)
      val (Branch _ a _) = a
      val Nil = undefined
      level Nil = []
      level (Branch Nil _ Nil) = []
      level (Branch Nil _ b) = [b]
      level (Branch a _ Nil) = [a]
      level (Branch a _ b) = [a, b]

mkEndo :: Foldable t => t (a -> a) -> Endo a
mkEndo t = Endo $ foldr (.) id t

sum :: (Foldable t, Num a) => t a -> a
sum = getSum . foldMap Sum

product :: (Foldable t, Num a) => t a -> a
product = getProduct . foldMap Product

elem :: (Foldable t, Eq a) => a -> t a -> Bool
elem x = getAny . foldMap (Any . (== x))

minimum :: (Foldable t, Ord a) => t a -> Maybe a
minimum = foldr (comparingWith (<)) Nothing

maximum :: (Foldable t, Ord a) => t a -> Maybe a
maximum = foldr (comparingWith (>)) Nothing

comparingWith :: (a -> a -> Bool) -> a -> Maybe a -> Maybe a
comparingWith f x Nothing = Just x
comparingWith f x (Just x')
  | x `f` x' = Just x
  | otherwise = Just x'

null :: (Foldable t) => t a -> Bool
null = foldr (\_ _ -> False) True

length :: (Foldable t) => t a -> Int
length = foldr (const (+ 1)) 0

toList :: (Foldable t) => t a -> [a]
toList = foldr (:) []

fold :: (Foldable t, Monoid m) => t m -> m
fold = mconcat . toList

foldMap' :: (Foldable t, Monoid m) => (a -> m) -> t a -> m
foldMap' f = foldr ((<>) . f) mempty

newtype Constant a b =
  Constant a

instance Foldable (Constant a) where
  foldMap _ _ = mempty

data Two a b =
  Two a
      b

instance Foldable (Two x) where
  foldMap f (Two _ a) = f a

data Three a b c =
  Three a
        b
        c

instance Foldable (Three a b) where
  foldMap f (Three _ _ a) = f a

data Three' a b =
  Three' a
         b
         b

instance Foldable (Three' a) where
  foldMap f (Three' _ a a') = f a <> f a'

data Four' a b =
  Four' a
        b
        b
        b

instance Foldable (Four' a) where
  foldMap f (Four' _ a a' a'') = f a <> f a' <> f a''


--wtf
filterF :: (Applicative f, Foldable t, Monoid (f a)) => (a -> Bool) -> t a -> f a
filterF f = foldMap (\a -> if f a then pure a else mempty)  