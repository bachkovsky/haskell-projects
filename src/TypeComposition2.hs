{-# LANGUAGE InstanceSigs #-}

module TypeComposition2 where

import Control.Applicative
import Control.Monad

newtype Identity a = Identity
  { runIdentity :: a
  }

newtype Compose f g a = Compose
  { getCompose :: f (g a)
  }

cmpsExample :: Compose [] Maybe Integer
cmpsExample = Compose [Just 1, Nothing]

instance Functor Identity where
  fmap f (Identity a) = Identity (f a)

-- composition of two functors is also a functor
-- that's somtimes called "closed under composition"
instance (Functor f, Functor g) => Functor (Compose f g) where
  fmap f (Compose fga) = Compose $ (fmap . fmap) f fga

cmpsFmapExample :: Compose [] Maybe String
cmpsFmapExample = fmap show cmpsExample

instance (Applicative f, Applicative g) => Applicative (Compose f g) where
  pure = Compose . pure . pure
  (<*>) :: Compose f g (a -> b) -> Compose f g a -> Compose f g b
  -- f :: f (g (a -> b))
  -- v :: f (g (a))
  Compose f <*> Compose v = Compose $ liftA2 (<*>) f v

instance (Foldable f, Foldable g) => Foldable (Compose f g) where
  foldMap :: (Monoid m) => (a -> m) -> Compose f g a -> m
  foldMap f (Compose ts) = (foldMap . foldMap) f ts

instance (Traversable f, Traversable g) => Traversable (Compose f g) where
  traverse ::
       (Applicative ap) => (a -> ap b) -> Compose f g a -> ap (Compose f g b)
  traverse f (Compose c) = Compose <$> (traverse . traverse) f c

class Bifunctor p where
  {-# MINIMAL bimap | first, second #-}
  bimap :: (a -> b) -> (c -> d) -> p a c -> p b d
  bimap f g = first f . second g
  first :: (a -> b) -> p a c -> p b c
  first f = bimap f id
  second :: (b -> c) -> p a b -> p a c
  second = bimap id

data Deux a b =
  Deux a
       b

instance Bifunctor Deux where
  bimap f g (Deux a b) = Deux (f a) (g b)

newtype BConst a b =
  BConst a

instance Bifunctor BConst where
  bimap f _ (BConst a) = BConst (f a)

data Drei a b c =
  Drei a
       b
       c

instance Bifunctor (Drei a) where
  bimap f g (Drei a b c) = Drei a (f b) (g c)

data SuperDrei a b c =
  SuperDrei a
            b

instance Bifunctor (SuperDrei a) where
  bimap f _ (SuperDrei a b) = SuperDrei a (f b)

newtype SemiDrei a b c =
  SemiDrei a

instance Bifunctor (SemiDrei a) where
  bimap _ _ (SemiDrei a) = SemiDrei a

data Quadriceps a b c d =
  Quadzzz a
          b
          c
          d

instance Bifunctor (Quadriceps a b) where
  bimap f g (Quadzzz a b c d) = Quadzzz a b (f c) (g d)

data MEither a b
  = MLeft a
  | MRight b

instance Bifunctor MEither where
  bimap f _ (MLeft a) = MLeft (f a)
  bimap _ g (MRight b) = MRight (g b)
