{-# LANGUAGE OverloadedStrings #-}
module ParserPractice where

import Control.Applicative
import qualified Data.ByteString as BS
import qualified Data.List as L
import Data.Time
import Text.Parser.Combinators
import Text.Parser.LookAhead
import Text.Trifecta

stop :: Parser a
stop = unexpected "stop"

one :: Parser Char
one = char '1'

one' :: Parser ()
one' = one >> stop

oneTwo :: Parser ()
oneTwo = char '1' >> char '2' >> eof

testParse :: Show a => Parser a -> IO ()
testParse p = print $ parseString p mempty "123"

newtype Thread =
  Thread String
  deriving (Eq, Show)

newtype Host =
  Host String
  deriving (Eq, Show)

newtype Timestamp =
  Ts UTCTime
  deriving (Eq, Show)

newtype Url =
  Url String
  deriving (Eq, Show)

newtype User =
  User String
  deriving (Eq, Show)

newtype Ip =
  Ip String
  deriving (Eq, Show)

newtype Level =
  Lvl String
  deriving (Eq, Show)

newtype Logger =
  Log String
  deriving (Eq, Show)

newtype Message =
  Msg String
  deriving (Eq, Show)

data LogEvent = LogEvent
  { _thread :: Thread
  , _host :: Host
  , _time :: Timestamp
  , _url :: Url
  , _user :: User
  , _ip :: Maybe Ip
  , _level :: Level
  , _log :: Logger
  , _message :: Message
  } deriving (Eq, Show)

-- http-bio-63080-exec-1323#localhost 2018-07-29 23:32:51.634 [http://elearn.apteka-aprel.ru/mira suhareva_ya 2.94.4.249] DEBUG(DBSession): resultset.count=1
logEvent :: Parser LogEvent
logEvent =
  LogEvent <$> thread <*> host <*> timestamp <*> url <*> user <*> ip <*> level <*>
  logger <*> message
  where
    alphaNumeric = ['a' .. 'z'] ++ ['A' .. 'Z'] ++ ['0' .. '9']
    thread :: Parser Thread
    thread = Thread <$> some (oneOf threadSymbols) <* char '#'
    threadSymbols = '-' : alphaNumeric
    host :: Parser Host
    host = Host <$> some (oneOf urlSymbols) <* space
    timestamp :: Parser Timestamp
    timestamp = Ts <$> datetime <* string " ["
    url :: Parser Url
    url = Url <$> some (oneOf urlSymbols) <* space
    urlSymbols = '-' : '.' : ':' : '/' : alphaNumeric
    user :: Parser User
    user = User <$> some (oneOf userSymbols) <* skipMany space
    userSymbols = '@' : '_' : '-' : '.' : alphaNumeric
    ip :: Parser (Maybe Ip)
    ip = fmap Ip <$> optional (allUntil "]") <* string "] "
    level :: Parser Level
    level = Lvl <$> allUntil "("
    logger :: Parser Logger
    logger = Log <$> parens (some (oneOf loggerSymbols)) <* string ": "
    loggerSymbols = '.' : alphaNumeric
    -- messages can be multi-line, so we should look ahead 
    -- for new log event or eof to tell when message is over
    message :: Parser Message
    message = scanForMessage ""
      where
        scanForMessage m = do
          t <- optional $ try $ lookAhead thread
          e <- optional $ try $ lookAhead eof
          case (t, e) of
            -- neither eof nor new event, so append line and repeat
            (Nothing, Nothing) -> do
              l <- line'
              skipEOL
              scanForMessage (m `join` l)
            -- message ended
            _ -> return $ Msg m
        line' = allUntil "\n"
        skipEOL = skipMany (string "\n")
        join m m' =
          if L.null m
            then m'
            else L.intercalate "\n" [m, m']

allUntil :: CharParsing f => String -> f String
allUntil = some . noneOf

--  2018-07-29 23:32:51.634 
datetime :: Parser UTCTime
datetime = UTCTime <$> day <* space <*> diffTime

--  2018-07-29 
day :: Parser Day
day = fromGregorian <$> decimal |- decimalInt |- decimalInt

-- 23:32:51.634
diffTime :: Parser DiffTime
diffTime = time <$> decimal |: decimal |: decimal |. decimal
  where
    time h m s ms =
      let hps = h * 60 * 60 * 10 ^ 12
          mps = m * 60 * 10 ^ 12
          sps = s * 10 ^ 12
          msps = ms * 10 ^ 9
       in picosecondsToDiffTime (hps + mps + sps + msps)

infixl 4 |:

(|:) :: CharParsing f => f (a -> b) -> f a -> f b
(|:) = btw ':'

infixl 4 |.

(|.) :: CharParsing f => f (a -> b) -> f a -> f b
(|.) = btw '.'

infixl 4 |-

(|-) :: CharParsing f => f (a -> b) -> f a -> f b
(|-) = btw '-'

btw :: CharParsing f => Char -> f (a -> b) -> f a -> f b
btw c a b = a <* char c <*> b

decimalInt :: Parser Int
decimalInt = fromIntegral <$> decimal

sss :: Parser (Char, Char, Char)
sss = (,,) <$> digit <* char '.' <*> digit <* char '.' <*> digit

main :: IO ()
main = do
  bs <-
    BS.readFile
      "/home/mike/hdd/logs/rzd/PORTAL-42975/SystemOut_2018-05-31_07.44.16.781.log"
  let res = parseByteString (some logEvent) mempty bs
  print (length <$> res)