module Applicative where

import Control.Applicative

{-

Problem: it's impossible to generalize functor idea to functions of multiple arguments (2, 3, ...)

fmap2arg :: Functor f => (a -> (b -> c)) -> f a -> f b -> f c
fmap2arg g as bs =
    let ga = fmap g as  -- ga :: f (b -> c)
    in ga _ bs          -- bs :: f b

To deal with it, we need function that accepts function inside functor container (f (b -> c))
and applies it to values inside same container (f b)

apply :: f (b -> c) -> f b -> f c

Solution: Applicative typeclass

class Functor f => Applicative f where
    -- embeds something into applicative structure
    pure :: a -> f a

    -- applies container of functions to container of elements
    (<*>) :: f (a -> b) -> f a -> f b
    -- fmap
    (<$>) ::   (a -> b) -> f a -> f b


Useful functions in library:

liftA  :: Applicative f => (a -> b)           -> f a -> f b
liftA2 :: Applicative f => (a -> b -> c)      -> f a -> f b -> f c
liftA3 :: Applicative f => (a -> b -> c -> d) -> f a -> f b -> f c -> f d
-}
-- primitive validation example
nonEmpty :: String -> Either String String
nonEmpty "" = Left "String is empty"
nonEmpty s = Right s

positive :: (Show a, Ord a, Num a) => a -> Either String a
positive i
  | i > 0 = Right i
  | otherwise = Left $ "Number is negative: " ++ show i

bounded :: (Show a, Ord a) => a -> a -> a -> Either String a
bounded min' max' i
  | min' <= i && i <= max' = Right i
  | otherwise =
    Left $
    "Value " ++
    show i ++ " is not within bounds [" ++ show min' ++ ", " ++ show max' ++ "]"

-- Person constructor is function of multiple arguments
data Person = MakePerson
  { name :: String
  , age :: Int
  , weight :: Int
  } deriving (Eq, Show)

mkPerson :: String -> Int -> Int -> Either String Person
-- can use 'ap' for lifting:
-- mkPerson name age weight = Person <$> nonEmpty name <*> bounded 18 150 age <*> positive weight
-- or standard library:
mkPerson n a w =
  liftA3 MakePerson (nonEmpty n) (bounded 18 150 a) (positive w)

data List a
  = Nil
  | Cons a
         (List a)
  deriving (Eq, Show)

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons x xs) = Cons (f x) (fmap f xs)

instance Semigroup (List a) where
  Nil <> x = x
  x <> Nil = x
  Cons x xs <> ys = Cons x (xs <> ys)

instance Monoid (List a) where
  mempty = Nil
  mappend = (<>)

instance Applicative List where
  pure x = Cons x Nil
  _ <*> Nil = Nil
  Nil <*> _ = Nil
  (Cons f fs) <*> xs = (f <$> xs) <> (fs <*> xs)

take' :: Int -> List a -> List a
take' = take'' Nil
  where
    take'' res _ Nil = res
    take'' res 0 _ = res
    take'' res n (Cons x xs) = take'' (res <> Cons x Nil) (n - 1) xs

newtype ZipList' a =
  ZipList' (List a)
  deriving (Eq, Show)

instance Functor ZipList' where
  fmap f (ZipList' xs) = ZipList' $ fmap f xs

instance Applicative ZipList' where
  pure = ZipList' . repeat' where
    repeat' x = Cons x (repeat' x)
  (ZipList' l1) <*> (ZipList' l2) = ZipList' $ zipWith' ($) l1 l2
    where
      zipWith' _ Nil _ = Nil
      zipWith' _ _ Nil = Nil
      zipWith' f (Cons x xs) (Cons y ys) = Cons (x `f` y) (zipWith' f xs ys)
