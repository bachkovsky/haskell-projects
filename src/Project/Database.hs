module Project.Database
  ( getBudget
  , getTransactions
  ) where

import Control.Monad (liftM2)
import Data.Decimal (realFracToDecimal)
import Project.Project
import System.Random (getStdRandom, randomR)

getBudget :: ProjectId -> IO Budget
getBudget _ =
  let m = rndMoney (0, 10000)
   in liftM2 Budget m m

getTransactions :: ProjectId -> IO [Transaction]
getTransactions _ = do
  sale <- Sale <$> rndMoney (0, 4000)
  purchase <- Purchase <$> rndMoney (0, 4000)
  pure [sale, purchase]

rndMoney :: (Double, Double) -> IO Money
rndMoney range = Money . realFracToDecimal 2 <$> getStdRandom (randomR range)
