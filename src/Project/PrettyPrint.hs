{-# LANGUAGE OverloadedStrings #-}

module Project.PrettyPrint where

import Data.Decimal (roundTo)
import Data.Tree
import Text.Printf

import Project.Project
import Project.Reporting

asTree :: (g -> String) -> (a -> String) -> Project g a -> Tree String
asTree _ valueFormat (Project name val) =
  Node (printf "%s %s" name (valueFormat val)) []
asTree groupFormat valueFormat (ProjectGroup name x projects) =
  Node
    (printf "%s %s" name (groupFormat x))
    (map (asTree groupFormat valueFormat) projects)

prettyProject :: (g -> String) -> (a -> String) -> Project g a -> String
prettyProject groupFormat valueFormat =
  drawTree . asTree groupFormat valueFormat

prettyReport :: Report -> String
prettyReport (Report b n d) =
  printf
    "Budget: %s, Net: %s, Difference: %s"
    (prettyMoney b)
    (prettyMoney n)
    (prettyMoney d)

prettyId :: ProjectId -> String
prettyId (ProjectId id') = "[" ++ show id' ++ "]"

prettyMoney :: Money -> String
prettyMoney (Money d) = sign ++ show (roundTo 2 d)
  where
    sign =
      if d > 0
        then "+"
        else ""
