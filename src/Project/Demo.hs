{-# LANGUAGE OverloadedStrings #-}

module Project.Demo where

import Project.PrettyPrint
import Project.Project
import Project.Reporting

someProject :: Project () ProjectId
someProject = ProjectGroup "Sweden" () [stockholm, gothenburg, malmo]
  where
    stockholm = Project "Stockholm" 1
    gothenburg = Project "Gothenburg" 2
    malmo = ProjectGroup "Malmo" () [city, limhamn]
    city = Project "Malmo City" 3
    limhamn = Project "Limhamn" 4

defaultPrettyProject :: Project () ProjectId -> String
defaultPrettyProject = prettyProject (const "") prettyId

defaultPrettyProjectReport :: Project Report Report -> String
defaultPrettyProjectReport = prettyProject prettyReport prettyReport

printProject :: IO ()
printProject = (putStrLn . defaultPrettyProject) someProject

printProjectReport :: IO ()
printProjectReport =
  calculateProjectReport someProject >>= (putStrLn . defaultPrettyProjectReport)
