{-# LANGUAGE RankNTypes #-}

module Lenses.MyLensesDemo where

import Control.Monad.Identity
import Control.Applicative
import Data.Functor.Const
import Data.Char

data Person = P
  { _name :: String
  , _addr :: Address
  , _salary :: Int
  } deriving (Eq, Show)

data Address = A
  { _road :: String
  , _city :: String
  , _postcode :: String
  -- , _name :: String
  } deriving (Eq, Show)

setName :: String -> Person -> Person
setName n p = p {_name = n}

-- nested record update gets tiresome
setPostcode :: String -> Person -> Person
setPostcode pc p = p {_addr = (_addr p) {_postcode = pc}}

-- inefficient for modification (pattern match twice)
data LensR s a = L
  { viewR :: s -> a
  , setR :: a -> s -> s
  }

type Lens' s a
   = forall f. (Functor f) =>
                 (a -> f a) -> s -> f s

set :: Lens' s a -> a -> s -> s
set ln x = over ln (const x)

-- doesn't pattern match twice, as LensR would do
over :: Lens' s a -> (a -> a) -> s -> s
over ln f = runIdentity . ln (Identity . f)

view :: Lens' s a -> s -> a
view ln = getConst . ln Const

lensToLensR :: Lens' s a -> LensR s a
lensToLensR ln = L {viewR = view ln, setR = set ln}

lensRToLens :: LensR s a -> Lens' s a
lensRToLens (L viewR' setR') fa s = (`setR'` s) <$> fa (viewR' s)

name :: Lens' Person String
name elt_fn p@P {_name = n} = (\n' -> p {_name = n'}) <$> elt_fn n

addr :: Lens' Person Address
addr elt_fn p@P {_addr = a} = (\a' -> p {_addr = a'}) <$> elt_fn a

postcode :: Lens' Address String
postcode elt_fn (A r c p) = A r c <$> elt_fn p

main :: IO ()
main = do
  let fred =
        P
          { _name = "Fred"
          , _addr = A {_road = "Road 1", _city = "Moscow", _postcode = "123123"}
          , _salary = 120000
          }
  print fred
  print $ view addr fred
  print $ view (addr . postcode) fred
  print $ set (addr . postcode) "321321" fred
  print $ over' addrStrs (map toLower) (view addr fred)
  print $ view' addrStrs (view addr fred)

type Traversal' s a
   = forall f. (Applicative f) =>
                 (a -> f a) -> s -> f s

road :: Lens' Address String
road elt_fn (A r c p) = (\ r' -> A r' c p) <$> elt_fn r

addrStrs :: Traversal' Address String
addrStrs elt_fn (A r c p) = liftA2 (\ r' c' -> A r' c' p) (elt_fn r) (elt_fn c)

over' :: Traversal' s a -> (a -> a) -> s -> s
over' ln f = runIdentity . ln (Identity . f)

view' :: Monoid a => Traversal' s a -> s -> a
view' ln = getConst . ln Const
