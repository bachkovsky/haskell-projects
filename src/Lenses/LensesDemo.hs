{-# LANGUAGE TemplateHaskell #-}
module Lenses.LensesDemo where

import Control.Lens
import Control.Lens.TH

newtype Temp = T{_fahrenheit :: Float}
makeLenses ''Temp

centigrade :: Lens Temp Float
centigrade centi_fn (T faren) 
  =  T . cToF <$> centi_fn (fToC faren)

cToF :: Float -> Float
cToF = undefined

fToC :: Float -> Float
FToC = undefined

