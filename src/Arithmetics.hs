module Arithmetics where

add :: Num a => a -> a -> a
add = (+)

dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num denom 0
  where
    go n d count
      | n < d = (count, n)
      | otherwise = go (n - d) d (count + 1)

multiply :: (Eq a, Num a) => a -> a -> a
multiply 0 _ = 0
multiply _ 0 = 0
multiply 1 b = b
multiply a 1 = a
multiply a b = a + multiply a (b - 1)

isGreater :: (Ord a) => a -> a -> Bool
isGreater = (>)

isSmaller :: (Ord a) => a -> a -> Bool
isSmaller = (<)