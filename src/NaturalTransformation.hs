{-# LANGUAGE RankNTypes #-}

module NaturalTransformation where

-- have no information about "a" type
type Nat f g = forall a. f a -> g a

-- thereby there is no way to change value
maybeToList :: Nat Maybe []
maybeToList Nothing  = []
maybeToList (Just a) = [a]
