module ReplaceExperiment where


replaceWithP :: b -> Char
replaceWithP = const 'p'

lms :: [Maybe String]
lms = [Just "Ave", Nothing, Just "woohoo"]

-- Just making the argument more specific
replaceWithP' :: [Maybe String] -> Char
replaceWithP' = replaceWithP

liftedReplace :: Functor f => f a -> f Char
liftedReplace = fmap replaceWithP

liftedReplace' :: [Maybe String] -> String
liftedReplace' = liftedReplace

twiceLifted :: (Functor f1, Functor f) => f (f1 a) -> f (f1 Char)
twiceLifted = (fmap . fmap) replaceWithP

-- Making it more specific
twiceLifted' :: [Maybe String] -> [Maybe Char]
twiceLifted' = twiceLifted

thriceLifted :: (Functor f2, Functor f1, Functor f) => f (f1 (f2 a)) -> f (f1 (f2 Char))
thriceLifted = (fmap . fmap . fmap) replaceWithP

thriceLifted' :: [Maybe String] -> [Maybe String]
thriceLifted' = thriceLifted

main :: IO ()
main = do
  putStr "replaceWithP' lms:"
  print (replaceWithP' lms)
  putStr "liftedReplace lms:"
  print (liftedReplace lms)
  putStr "liftedReplace' lms:"
  print (liftedReplace' lms)
  putStr "twiceLifted lms:"
  print (twiceLifted lms)
  putStr "twiceLifted' lms:"
  print (twiceLifted' lms)
  putStr "thriceLifted lms:"
  print (thriceLifted lms)
  putStr "thriceLifted' lms:"
  print (thriceLifted' lms)

a :: [Int]
a = ((+ 1) <$> read "[1]") :: [Int]

b :: Maybe [String]
b = (fmap . fmap) (++ "lol") (Just ["Hi,", "Hello"])

c :: Integer -> Integer
c = fmap (* 2) (\x -> x - 2)

d :: Integer -> String
d = fmap ((return '1' ++) . show) (\x -> [x,1 .. 3])

e :: IO Integer
e =
  let ioi = readIO "1" :: IO Integer
      changed = fmap (read . ("123" ++) . show) ioi
   in fmap (* 3) changed

--      changed = ((read . ("123" ++)) <$> fmap show ioi)
--      changed = fmap (read . ("123" ++)) $ fmap show ioi