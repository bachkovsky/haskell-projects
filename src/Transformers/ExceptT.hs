{-# LANGUAGE InstanceSigs #-}

module Transformers.ExceptT where

import Control.Applicative (liftA2)
import Data.Either
import Text.Read (readEither)
import Control.Arrow
import Transformers.MonadTrans
import Control.Monad.Trans.Writer
import Data.Monoid
import Data.Foldable

newtype Except e a = Except
  { runExcept :: Either e a
  }

newtype ExceptT e m a = ExceptT
  { runExceptT :: m (Either e a)
  }

except :: Monad m => Either e a -> ExceptT e m a
except = ExceptT . return

instance Functor (Except e) where
  fmap f = Except . fmap f . runExcept

instance Functor m => Functor (ExceptT e m) where
  fmap f = ExceptT . fmap (fmap f) . runExceptT

instance Applicative (Except e) where
  pure = Except . Right
  f <*> v = Except $ runExcept f <*> runExcept v

{-
не прерывает вычисления при ошибке

instance Applicative m => Applicative (ExceptT e m) where
  pure = ExceptT . pure . Right
  f <*> v = ExceptT $ liftA2 (<*>) (runExceptT f) (runExceptT v)
-}
instance Monad m => Applicative (ExceptT e m) where
  pure = ExceptT . pure . Right
  ExceptT mef <*> ExceptT mea =
    ExceptT $ do
      ef <- mef
      case ef of
        Left e -> return (Left e)
        Right f -> (fmap . fmap) f mea

instance Monad (Except e) where
  m >>= k =
    Except $
    case runExcept m of
      Left e -> Left e
      Right x -> runExcept (k x)

instance Monad m => Monad (ExceptT e m) where
  m >>= k =
    ExceptT $ do
      a <- runExceptT m
      case a of
        Left e -> return (Left e)
        Right x -> runExceptT (k x)
  fail = ExceptT . fail

data Tile
  = Floor
  | Chasm
  | Snake
  deriving (Show)

data DeathReason
  = Fallen
  | Poisoned
  deriving (Eq, Show)

type Point = (Integer, Integer)

type GameMap = Point -> Tile

moves :: GameMap -> Int -> Point -> [Either DeathReason Point]
moves = go []
  where
    go results game n p@(x, y)
      | n < 0 = results
      | otherwise =
        case game p of
          Floor ->
            let goNext =
                  concatMap
                    (go results game (n - 1))
                    [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
             in if n == 0
                  then Right p : goNext
                  else goNext
          Chasm -> Left Fallen : results
          Snake -> Left Poisoned : results

waysToDie :: DeathReason -> GameMap -> Int -> Point -> Int
waysToDie r m n p = length $ filter (== r) $ lefts $ moves m n p

map1 :: GameMap
map1 (2, 2) = Snake
map1 (4, 1) = Snake
map1 (x, y)
  | 0 < x && x < 5 && 0 < y && y < 5 = Floor
  | otherwise = Chasm

instance MonadTrans (ExceptT e) where
  lift = ExceptT . fmap Right

throwE :: Monad m => e -> ExceptT e m a
throwE = ExceptT . return . Left

catchE :: Monad m => ExceptT e m a -> (e -> ExceptT e' m a) -> ExceptT e' m a
m `catchE` h =
  ExceptT $ do
    a <- runExceptT m
    case a of
      Left l -> runExceptT (h l)
      Right r -> return (Right r)


data ReadError
  = EmptyInput
  | NoParse String
  deriving (Show)

tryRead :: (Read a, Monad m) => String -> ExceptT ReadError m a
tryRead "" = throwE EmptyInput
tryRead s = except $ left NoParse $ readEither s

data Tree a = Leaf a | Fork (Tree a) a (Tree a)


treeSum :: Foldable t => t String -> (Maybe ReadError, Integer)
treeSum t = let (err, s) = runWriter . runExceptT $ traverse_ go t
            in (maybeErr err, getSum s)
  where
    maybeErr :: Either ReadError () -> Maybe ReadError
    maybeErr = either Just (const Nothing)

instance Foldable Tree where
  foldr f ini (Leaf a) = f a ini
  foldr f ini (Fork l x r) = foldr f (f x (foldr f ini r)) l

go :: String -> ExceptT ReadError (Writer (Sum Integer)) ()
go val = tryRead val >>= lift . tell . Sum