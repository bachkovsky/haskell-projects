module Transformers.Demo where

import Control.Monad.Identity
import Data.Char (toUpper)
import Transformers.MonadTrans
import Transformers.ReaderT
import Transformers.WriterT

logFirstAndRetSecond :: ReaderT [String] (WriterT String Identity) String
logFirstAndRetSecond = do
  el1 <- asks head
  el2 <- asks (map toUpper . head . tail)
  lift $ tell el1
  return el2
