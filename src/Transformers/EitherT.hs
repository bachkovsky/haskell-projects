{-# LANGUAGE InstanceSigs #-}

module Transformers.EitherT where

import Control.Applicative
import Transformers.MonadTrans

newtype EitherT e m a = EitherT
  { runEitherT :: m (Either e a)
  }

instance Functor m => Functor (EitherT e m) where
  fmap f = EitherT . (fmap . fmap) f . runEitherT

instance Applicative m => Applicative (EitherT e m) where
  pure = EitherT . pure . Right
  (EitherT f) <*> (EitherT a) = EitherT $ liftA2 (<*>) f a

instance Monad m => Monad (EitherT e m) where
  return = pure
  (>>=) :: EitherT e m a -> (a -> EitherT e m b) -> EitherT e m b
  (EitherT v) >>= k =
    EitherT $ do
      v' <- v
      case v' of
        (Left e) -> return (Left e)
        (Right a) -> runEitherT (k a)
  fail = EitherT . fail

swapEither :: Either e a -> Either a e
swapEither (Left a) = Right a
swapEither (Right b) = Left b

swapEitherT :: (Functor m) => EitherT e m a -> EitherT a m e
swapEitherT = EitherT . fmap swapEither . runEitherT

instance MonadTrans (EitherT e) where
  lift :: Monad m => m a -> EitherT e m a
  lift = EitherT . fmap Right

eitherT :: Monad m => (a -> m c) -> (b -> m c) -> EitherT a m b -> m c
eitherT f g e = runEitherT e >>= either f g