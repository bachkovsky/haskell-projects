{-# LANGUAGE InstanceSigs, TupleSections #-}

module Transformers.WriterT where

import Control.Applicative (liftA2)
import Control.Monad (ap)
import Data.Functor.Identity
import Data.Tuple (swap)
import Transformers.MonadTrans

newtype Writer w a = Writer
  { runWriter :: (a, w)
  }

newtype WriterT w m a = WriterT
  { runWriterT :: m (a, w)
  }

runExample :: Maybe (Integer, String)
runExample = runWriterT $ WriterT $ Just (42, "Hello!")

writer :: Monad m => (a, w) -> WriterT w m a
writer = WriterT . return

runExampleList :: [(Int, String)]
runExampleList = runWriterT (writer (42, "Hello"))

runExampleMaybe :: Maybe (Int, String)
runExampleMaybe = runWriterT (writer (42, "Hello"))

execWriter :: Writer w a -> w
execWriter = snd . runWriter

execWriterT :: Monad m => WriterT w m a -> m w
execWriterT = fmap snd . runWriterT

instance Functor (Writer w) where
  fmap :: (a -> b) -> Writer w a -> Writer w b
  fmap f = Writer . updater . runWriter
    where
      updater ~(x, log) = (f x, log)

instance Functor m => Functor (WriterT w m) where
  fmap :: (a -> b) -> WriterT w m a -> WriterT w m b
  fmap f = WriterT . fmap updater . runWriterT
    where
      updater ~(x, log) = (f x, log)

instance Monoid w => Applicative (Writer w) where
  pure x = Writer (x, mempty)
  f <*> v = Writer $ updater (runWriter f) (runWriter v)
    where
      updater ~(g, w) ~(x, w') = (g x, w <> w')

instance (Monoid w, Applicative m) => Applicative (WriterT w m) where
  pure x = WriterT $ pure (x, mempty)
  f <*> v = WriterT $ liftA2 updater (runWriterT f) (runWriterT v)
    where
      updater ~(g, w) ~(x, w') = (g x, w <> w')

instance Monoid w => Monad (Writer w) where
  m >>= k =
    Writer $
    let (v, w) = runWriter m
        (v', w') = runWriter (k v)
     in (v', w <> w')

instance (Monoid w, Monad m) => Monad (WriterT w m) where
  m >>= k =
    WriterT $ do
      ~(v, w) <- runWriterT m
      ~(v', w') <- runWriterT (k v)
      return (v', w <> w')
  fail :: String -> WriterT w m a
  fail = WriterT . fail


instance (Monoid w) => MonadTrans (WriterT w) where
  lift :: Monad m => m a -> WriterT w m a
  lift m = WriterT ((, mempty) <$> m)

tell :: Monad m => w -> WriterT w m ()
tell = writer . ((),)

listen :: Monad m => WriterT w m a -> WriterT w m (a, w)
listen m = WriterT $ do
  ~(a, w) <- runWriterT m
  return ((a, w), w)

censor :: Monad m => (w -> w) -> WriterT w m a -> WriterT w m a
censor f m = WriterT $ do
  ~(a, w) <- runWriterT m
  return (a, f w)
