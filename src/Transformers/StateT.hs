{-# LANGUAGE InstanceSigs, TupleSections, FlexibleContexts #-}

module Transformers.StateT where

import Transformers.MonadTrans
import Transformers.ReaderT
import Transformers.WriterT
import Data.Monoid

newtype State s a = State
  { runState :: s -> (a, s)
  }

newtype StateT s m a = StateT
  { runStateT :: s -> m (a, s)
  }

state :: Monad m => (s -> (a, s)) -> StateT s m a
state f = StateT (return . f)

evalStateT :: Monad m => StateT s m a -> s -> m a
evalStateT = (fmap . fmap) fst . runStateT

execStateT :: Monad m => StateT s m a -> s -> m s
execStateT = (fmap . fmap) snd . runStateT

readerToStateT :: Monad m => ReaderT r m a -> StateT r m a
readerToStateT r = StateT $ \s -> fmap (, s) (runReaderT r s)

get :: (Monad m) => StateT s m s
get = state $ \ s -> (s, s)

put :: (Monad m) => s -> StateT s m ()
put s = state $ const ((), s)

instance Functor (State s) where
  fmap f m = State $ \st -> updater $ runState m st
    where
      updater ~(x, s) = (f x, s)

instance Functor m => Functor (StateT s m) where
  fmap f m = StateT (fmap updater . runStateT m)
    where
      updater ~(x, s) = (f x, s)

instance Applicative (State s) where
  pure x = State (x, )
  f <*> v =
    State $ \s ->
      let (g, s') = runState f s
          (x, s'') = runState v s'
       in (g x, s'')

instance Monad m => Applicative (StateT s m) where
  pure x = StateT $ return . (x, )
  f <*> v =
    StateT $ \s -> do
      ~(g, s') <- runStateT f s
      ~(x, s'') <- runStateT v s'
      return (g x, s'')

instance Monad (State s) where
  m >>= k =
    State $ \s ->
      let (x, s') = runState m s
       in runState (k x) s'

instance Monad m => Monad (StateT s m) where
  m >>= k =
    StateT $ \s -> do
      ~(x, s') <- runStateT m s
      runStateT (k x) s'
  fail = StateT . const . fail

sl2 :: StateT Integer [] Integer
sl2 = StateT $ \st -> [(st, st), (st + 1, st - 1)]

x1 :: [((), Integer)]
x1 =
  runStateT
    (do 6 <- sl2
        return ())
    5

sm :: StateT Integer Maybe Integer
sm = StateT $ \st -> Just (st + 1, st - 1)

x2 :: Maybe ((), Integer)
x2 =
  runStateT
    (do 42 <- sm
        return ())
    5

instance MonadTrans (StateT s) where
  lift :: Monad m => m a -> StateT s m a
  lift m = StateT $ \st -> (, st) <$> m
  