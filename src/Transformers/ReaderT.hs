{-# LANGUAGE InstanceSigs, FlexibleContexts #-}

module Transformers.ReaderT where

import Control.Applicative
import Control.Monad
import Transformers.MonadTrans

newtype Reader r a = Reader
  { runReader :: r -> a
  }

newtype ReaderT r m a = ReaderT
  { runReaderT :: r -> m a
  }

reader :: Monad m => (r -> a) -> ReaderT r m a
reader f = ReaderT (return . f)

asks :: Monad m => (r -> a) -> ReaderT r m a
asks = reader

newtype Arr2T e1 e2 m a = Arr2T
  { getArr2T :: e1 -> e2 -> m a
  }

newtype Arr3T e1 e2 e3 m a = Arr3T
  { getArr3T :: e1 -> e2 -> e3 -> m a
  }

arr2 :: Monad m => (e1 -> e2 -> a) -> Arr2T e1 e2 m a
arr2 = Arr2T . ((return .) .)

arr3 :: Monad m => (e1 -> e2 -> e3 -> a) -> Arr3T e1 e2 e3 m a
arr3 = Arr3T . (((return .) .) .)

instance Functor (Reader r) where
  fmap :: (a -> b) -> Reader r a -> Reader r b
  fmap f rdr = Reader $ f . runReader rdr

instance Functor m => Functor (ReaderT r m) where
  fmap :: (a -> b) -> ReaderT r m a -> ReaderT r m b
  fmap f rdr = ReaderT $ fmap f . runReaderT rdr

instance Functor m => Functor (Arr2T e1 e2 m) where
  fmap :: (a -> b) -> Arr2T e1 e2 m a -> Arr2T e1 e2 m b
  fmap f (Arr2T g) = Arr2T $ (fmap . fmap) f . g

instance Functor m => Functor (Arr3T e1 e2 e3 m) where
  fmap :: (a -> b) -> Arr3T e1 e2 e3 m a -> Arr3T e1 e2 e3 m b
  fmap f (Arr3T g) = Arr3T $ (fmap . fmap . fmap) f . g

instance Applicative m => Applicative (Arr2T e1 e2 m) where
  pure :: a -> Arr2T e1 e2 m a
  pure = Arr2T . const . const . pure
  (<*>) :: Arr2T e1 e2 m (a -> b) -> Arr2T e1 e2 m a -> Arr2T e1 e2 m b
  (Arr2T f) <*> (Arr2T a) = Arr2T $ \e1 e2 -> f e1 e2 <*> a e1 e2

instance Monad m => Monad (Arr2T e1 e2 m) where
  (>>=) :: Arr2T e1 e2 m a -> (a -> Arr2T e1 e2 m b) -> Arr2T e1 e2 m b
  (Arr2T a) >>= k =
    Arr2T $ \e1 e2 -> do
      a' <- a e1 e2
      getArr2T (k a') e1 e2

instance Monad m => Monad (Arr3T e1 e2 e3 m) where
  (>>=) :: Arr3T e1 e2 e3 m a -> (a -> Arr3T e1 e2 e3 m b) -> Arr3T e1 e2 e3 m b
  (Arr3T a) >>= k =
    Arr3T $ \e1 e2 e3 -> do
      a' <- a e1 e2 e3
      getArr3T (k a') e1 e2 e3

instance Applicative m => Applicative (Arr3T e1 e2 e3 m) where
  pure = Arr3T . const . const . const . pure
  (Arr3T f) <*> (Arr3T a) = Arr3T $ \e1 e2 e3 -> f e1 e2 e3 <*> a e1 e2 e3

instance Applicative (Reader r) where
  pure :: a -> Reader r a
  pure = Reader . const
  (<*>) :: Reader r (a -> b) -> Reader r a -> Reader r b
  (Reader f) <*> (Reader e) = Reader $ \r -> f r (e r)

instance Applicative m => Applicative (ReaderT r m) where
  pure :: Applicative f => a -> ReaderT r f a
  pure = ReaderT . const . pure
  (<*>) :: ReaderT r m (a -> b) -> ReaderT r m a -> ReaderT r m b
  (ReaderT f) <*> (ReaderT a) = ReaderT $ liftA2 (<*>) f a

instance Monad (Reader r) where
  (>>=) :: Reader r a -> (a -> Reader r b) -> Reader r b
  m >>= k =
    Reader $ \e ->
      let v = runReader m e
       in runReader (k v) e

instance Monad m => Monad (ReaderT r m) where
  m >>= k =
    ReaderT $ \env -> do
      v <- runReaderT m env
      runReaderT (k v) env

{-
1) lift . return === return
2) lift (m >>= k) === lift m >>= (lift . k)
-}
instance MonadTrans (ReaderT r) where
  lift :: m a -> ReaderT r m a
  lift m = ReaderT (const m)

instance MonadTrans (Arr2T e1 e2)
  -- lift :: m a -> Arr2T e1 e2 m a
                                    where
  lift m = Arr2T $ (const . const) m

asks2 :: Monad m => (e1 -> e2 -> a) -> Arr2T e1 e2 m a
asks2 f = Arr2T $ \e1 e2 -> return (f e1 e2)

sss :: IO (Char, Char, (Char, Char))
sss =
  getArr2T
    (do x <- asks2 const
        y <- asks2 (flip const)
        z <- asks2 (,)
        return (x, y, z))
    'A'
    'B'
