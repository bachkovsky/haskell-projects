{-# LANGUAGE InstanceSigs, TupleSections #-}
module Transformers.LoggT where

import Control.Monad.Identity
import Transformers.MonadTrans

data Logged a =
  Logged String
         a
  deriving (Eq, Show)

newtype LoggT m a = LoggT
  { runLoggT :: m (Logged a)
  }

instance Functor Logged where
  fmap f (Logged s a) = Logged s (f a)

instance Functor m => Functor (LoggT m) where
  fmap :: (a -> b) -> LoggT m a -> LoggT m b
  fmap f = LoggT . (fmap . fmap) f . runLoggT

instance Monad m => Applicative (LoggT m) where
  pure = return
  (<*>) = ap

instance Monad m => Monad (LoggT m) where
  return x = LoggT $ return (Logged "" x)
  m >>= k =
    LoggT $ do
      (Logged s a) <- runLoggT m
      (Logged s' b) <- runLoggT (k a)
      return (Logged (s <> s') b)
  fail = LoggT . fail

logTst :: LoggT Identity Integer
logTst = do
  x <- LoggT $ Identity $ Logged "AAA" 30
  let y = 10
  z <- LoggT $ Identity $ Logged "BBB" 2
  return $ x + y + z

failTst :: [Integer] -> LoggT [] Integer
failTst xs = do
  5 <- LoggT $ fmap (Logged "") xs
  LoggT [Logged "A" ()]
  return 42

write2log :: Monad m => String -> LoggT m ()
write2log s = LoggT $ return (Logged s ())

type Logg = LoggT Identity

runLogg :: Logg a -> Logged a
runLogg = runIdentity . runLoggT

instance MonadTrans LoggT where
  lift :: Monad m => m a -> LoggT m a
  lift m = LoggT (Logged "" <$> m)