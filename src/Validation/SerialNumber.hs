module Validation.SerialNumber
  ( SerialNumber
  , ValidationError(..)
  , makeSerialNumber
  , renderSerialNumber
  ) where

import qualified Data.HashSet as HashSet
import Data.HashSet (HashSet)
import qualified Data.Text as Text
import Data.Text (Text)
import Data.Validation

newtype SerialNumber =
  SerialNumber [Text]
  deriving (Eq, Show)

data ValidationError
  = WrongNumberOfGroups Int
  | InvalidGroupLength Int
                       Text
  | InvalidCharacters (HashSet Char)
                      Text
  deriving (Eq, Show)

validCharacters :: HashSet Char
validCharacters = HashSet.fromList (['A' .. 'Z'] ++ ['0' .. '9'])

sep :: Text
sep = Text.singleton '-'

makeSerialNumber :: Text -> Validation [ValidationError] SerialNumber
makeSerialNumber t =
  let groups = Text.splitOn sep t
   in if length groups == 4
        then SerialNumber <$> traverse validateGroup groups
        else Failure [WrongNumberOfGroups (length groups)]
  where
    validateGroup group
      | len /= 4 = Failure [InvalidGroupLength len group]
      | not (HashSet.null invalidChars) =
        Failure [InvalidCharacters invalidChars group]
      | otherwise = Success group
      where
        len = Text.length group
        invalidChars =
          HashSet.difference
            (HashSet.fromList (Text.unpack group))
            validCharacters

renderSerialNumber :: SerialNumber -> Text
renderSerialNumber (SerialNumber gs) = Text.intercalate sep gs
