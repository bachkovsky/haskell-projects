{-# LANGUAGE OverloadedStrings #-}

module Fractions where

import Control.Applicative
import Data.Ratio ((%))
import Text.Parser.Combinators
import Text.Trifecta

badFraction :: String
badFraction = "1/0"

alsoBad :: String
alsoBad = "10"

shouldWork :: String
shouldWork = "1/2"

shouldAlsoWork :: String
shouldAlsoWork = "2/1"

rational :: Parser Rational
rational = do
  numerator <- decimal
  _ <- many (char ' ')
  _ <- char '/'
  _ <- many (char ' ')
  denominator <- decimal
  case denominator of
    0 -> fail "Denominator cannot be zero"
    _ -> return (numerator % denominator)

rationalOrFractional :: Fractional a => Parser (Either Rational a)
rationalOrFractional = try (Left <$> rational) <|> (Right <$> fractional)

fractional :: Fractional a => Parser a
fractional = do
  i' <- decimal
  let i = fromIntegral i'
  maybeDot <- optional (char '.')
  case maybeDot of
    Nothing -> return i
    _ -> do
      f <- decimal
      return $ i + fromIntegral f * 10 ^^ (-length (show f))

main :: IO ()
main = do
  print $ parseString rational mempty shouldWork
  print $ parseString rational mempty shouldAlsoWork
  print $ parseString rational mempty alsoBad
  print $ parseString rational mempty badFraction
