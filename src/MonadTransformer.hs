module MonadTransformer where

import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.Reader
import Control.Monad.Trans.State
import Control.Monad.Trans.Writer
import Data.Char (toUpper)
import Data.List (partition)

secondElem :: Reader [String] String
secondElem = asks (head . tail)

logFirstReturnSecond :: [String] -> Writer String String
logFirstReturnSecond xs = do
  let el1 = head xs
  let el2 = (head . tail) xs
  tell el1
  return el2

type MyRW = ReaderT [String] (Writer String)

logFirstReadSecond :: MyRW String
logFirstReadSecond = do
  el1 <- myAsks head
  el2 <- myAsks (map toUpper . head . tail)
  myTell el1
  return el2

myAsks :: ([String] -> a) -> MyRW a
myAsks = asks

myTell :: String -> MyRW ()
myTell = lift . tell

test1 :: (String, String)
test1 = runWriter (runReaderT logFirstReadSecond ["1", "2", "3"])

logFirstAndRetSecond :: WriterT String (Reader [String]) String
logFirstAndRetSecond = do
  el1 <- lift $ asks head
  el2 <- lift $ asks (map toUpper . head . tail)
  _ <- tell el1
  return el2

separate :: (a -> Bool) -> (a -> Bool) -> [a] -> WriterT [a] (Writer [a]) [a]
separate p1 p2 xs = do
  tell $ filter p1 xs
  lift $ tell $ filter p2 xs
  return $ filter (liftM2 (&&) (not . p1) (not . p2)) xs

runMyRW :: MyRW a -> [String] -> (a, String)
runMyRW rw env = runWriter $ runReaderT rw env

type MyRWT m = ReaderT [String] (WriterT String m)

runMyRWT :: MyRWT m a -> [String] -> m (a, String)
runMyRWT rwt env = runWriterT $ runReaderT rwt env

myAsks' :: Monad m => ([String] -> a) -> MyRWT m a
myAsks' = asks

myTell' :: Monad m => String -> MyRWT m ()
myTell' = lift . tell

myLift' :: Monad m => m a -> MyRWT m a
myLift' = lift . lift

logFirstAndRetSecond' :: MyRWT IO String
logFirstAndRetSecond' = do
  el1 <- myAsks' head
  myLift' $ putStrLn $ "First is " ++ show el1
  el2 <- myAsks' (map toUpper . head . tail)
  myLift' $ putStrLn $ "Second is " ++ show el2
  myTell' el1
  return el2

veryComplexComputation :: MyRWT Maybe (String, String)
veryComplexComputation = do
  xs <- ask
  case partition (even . length) xs of
    (even1:even2:_, odd1:odd2:_) ->
      myTell' even1 >> myTell' "," >> myTell' odd1 >>
      return (map toUpper even2, map toUpper odd2)
    _ -> myLift' Nothing

tickCollatz :: State Integer Integer
tickCollatz = do
  n <- get
  let res =
        if odd n
          then 3 * n + 1
          else n `div` 2
  put res
  return n

type EsSi = ExceptT String (State Integer)

runEsSi :: EsSi a -> Integer -> (Either String a, Integer)
runEsSi = runState . runExceptT

go' :: Integer -> Integer -> State Integer Integer -> EsSi ()
go' low high s = do
  _ <- lift s
  cur <- lift get
  when (cur <= low) (throwE "Lower bound")
  when (cur >= high) (throwE "Upper bound")

tickCollatz' :: StateT Integer IO Integer
tickCollatz' = do
  n <- get
  let res =
        if odd n
          then 3 * n + 1
          else n `div` 2
  lift $ print res
  put res
  return n

type RiiEsSiT m = ReaderT (Integer, Integer) (ExceptT String (StateT Integer m))

runRiiEsSiT ::
     ReaderT (Integer, Integer) (ExceptT String (StateT Integer m)) a
  -> (Integer, Integer)
  -> Integer
  -> m (Either String a, Integer)
runRiiEsSiT r env = runStateT (runExceptT (runReaderT r env))

go :: Monad m => StateT Integer m Integer -> RiiEsSiT m ()
go s = do
  (low, high) <- ask
  _ <- (lift . lift) s
  next <- (lift . lift) get
  when (next <= low) (lift $ throwE "Lower bound")
  when (next >= high) (lift $ throwE "Upper bound")
