{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs      #-}

module Functors where

data Point3D a =
  Point3D a
          a
          a
  deriving (Show)

instance Functor Point3D where
  fmap f (Point3D x y z) = Point3D (f x) (f y) (f z)

data GeomPrimitive a
  = Point (Point3D a)
  | LineSegment (Point3D a)
                (Point3D a)
  deriving (Show)

instance Functor GeomPrimitive where
  fmap f (Point p)           = Point (fmap f p)
  fmap f (LineSegment p1 p2) = LineSegment (fmap f p1) (fmap f p2)

data Tree' a
  = Leaf' a
  | Branch' (Tree' a)
            a
            (Tree' a)
  deriving (Show)

testTree :: Tree' Integer
testTree = Branch' (Leaf' 2) 3 (Leaf' 4)

instance Functor Tree' where
  fmap g (Leaf' x)       = Leaf' (g x)
  fmap g (Branch' l x r) = Branch' (fmap g l) (g x) (fmap g r)

data Tree a
  = Leaf (Maybe a)
  | Branch (Tree a)
           (Maybe a)
           (Tree a)
  deriving (Show)

instance Functor Tree where
  fmap f (Leaf m)       = Leaf (f <$> m)
  fmap f (Branch l m r) = Branch (f <$> l) (f <$> m) (f <$> r)

--instance Functor ((,) s) where
--    fmap g (x, y) = (x, g y)
data Entry k1 k2 v =
  Entry (k1, k2)
        v
  deriving (Show)

newtype Map k1 k2 v =
  Map [Entry k1 k2 v]
  deriving (Show)

instance Functor (Entry k1 k2) where
  fmap g (Entry keys v) = Entry keys (g v)

instance Functor (Map k1 k2) where
  fmap g (Map es) = Map $ map (fmap g) es
    {-Законы:
    fmap id      = id
    fmap (p . q) = (fmap p) . (fmap q)
    -}

newtype Arr x y = Arr
  { getArr :: x -> y
  }

instance Functor (Arr x) where
  fmap f (Arr g) = Arr (f . g)

newtype Arr2 e1 e2 a = Arr2
  { getArr2 :: e1 -> e2 -> a
  }

newtype Arr3 e1 e2 e3 a = Arr3
  { getArr3 :: e1 -> e2 -> e3 -> a
  }

instance Functor (Arr2 e1 e2) where
  fmap g (Arr2 h) = Arr2 $ (g .) . h
  -- fmap g (Arr2 h) = Arr2 $ \e1 e2 -> g (h e1 e2)

instance Functor (Arr3 e1 e2 e3) where
  fmap g (Arr3 h) = Arr3 $ ((g .) .) . h
  -- fmap g (Arr3 h) = Arr3 $ \e1 e2 e3 -> g (h e1 e2 e3)

instance Applicative (Arr2 e1 e2) where
  pure x = Arr2 $ \_ _ -> x
  (Arr2 f) <*> (Arr2 g) = Arr2 $ \e1 e2 -> f e1 e2 (g e1 e2)

instance Applicative (Arr3 e1 e2 e3) where
  pure x = Arr3 $ \_ _ _ -> x
  (Arr3 f) <*> (Arr3 g) = Arr3 $ \e1 e2 e3 -> f e1 e2 e3 (g e1 e2 e3)

data WhoCares a
  = ItDoesnt
  | Matter a
  | WhatThisIsCalled
  deriving (Eq, Show)

instance Functor WhoCares where
  fmap f (Matter a)       = Matter (f a)
  fmap _ ItDoesnt         = ItDoesnt
  fmap _ WhatThisIsCalled = WhatThisIsCalled

data Two a b =
  Two a
      b
  deriving (Eq, Show)

data Or a b
  = First a
  | Second b
  deriving (Eq, Show)

instance Functor (Two a) where
  fmap f (Two a b) = Two a (f b)

instance Functor (Or a) where
  fmap _ (First a)  = First a
  fmap f (Second b) = Second (f b)

newtype Mu f = InF
  { outF :: f (Mu f)
  }

data Sum b a
  = FFirst a
  | SSecond b

instance Functor (Sum e) where
  fmap f (FFirst a)  = FFirst (f a)
  fmap _ (SSecond b) = SSecond b

data Company a c b
  = DeepBlue a
             c
  | Something b

instance Functor (Company e e') where
  fmap f (Something b)  = Something (f b)
  fmap _ (DeepBlue a c) = DeepBlue a c

data More b a
  = L a
      b
      a
  | R b
      a
      b
  deriving (Eq, Show)

instance Functor (More x) where
  fmap f (L a b a') = L (f a) b (f a')
  fmap f (R b a b') = R b (f a) b'

data Quant a b
  = Finance
  | Desk a
  | Bloor b

instance Functor (Quant a) where
  fmap _ Finance = Finance
  fmap _ (Desk a)  = Desk a
  fmap f (Bloor b) = Bloor (f b)

newtype KKK a b =
  KKK a

instance Functor (KKK a) where
  fmap _ (KKK a) = KKK a

newtype Flip f a b =
  Flip (f b a)
  deriving (Eq, Show)

newtype KK a b =
  KK a
  deriving (Eq, Show)

instance Functor (Flip KK a) where
  fmap f (Flip (KK a)) = Flip (KK (f a))

newtype EvilGoateeConst a b =
  GoatyConst b
  deriving (Eq, Show)

instance Functor (EvilGoateeConst a) where
  fmap f (GoatyConst b) = GoatyConst (f b)

newtype LiftItOut f a =
  LiftItOut (f a)

instance (Functor f) => Functor (LiftItOut f) where
  fmap f (LiftItOut functor) = LiftItOut (fmap f functor)

data Parappa f g a =
  DaWrappa (f a)
           (g a)

instance (Functor f, Functor g) => Functor (Parappa f g) where
  fmap :: (a -> b) -> Parappa f g a -> Parappa f g b
  fmap f (DaWrappa f' g') = DaWrappa (fmap f f') (fmap f g')

data IgnoreOne f g a b =
  IgnoringSomething (f a)
                    (g b)

instance (Functor g) => Functor (IgnoreOne f g a) where
  fmap :: (b -> c) -> IgnoreOne f g a b -> IgnoreOne f g a c
  fmap f (IgnoringSomething f' g') = IgnoringSomething f' (fmap f g')

data Notorious g o a t =
  Notorious (g o)
            (g a)
            (g t)

instance (Functor g) => Functor (Notorious g o a) where
  fmap f (Notorious o' a' t') = Notorious o' a' (f <$> t')

data List a
  = Nil
  | Cons a
         (List a)

instance Functor List where
  fmap _ Nil         = Nil
  fmap f (Cons x xs) = Cons (f x) (fmap f xs)

data GoatLord a
  = NoGoat
  | OneGoat a
  | MoreGoats (GoatLord a)
              (GoatLord a)
              (GoatLord a)

instance Functor GoatLord where
  fmap _ NoGoat = NoGoat
  fmap f (OneGoat g) = OneGoat (f g)
  fmap f (MoreGoats gl1 gl2 gl3) = MoreGoats (f <$> gl1) (f <$> gl2) (f <$> gl3)

data TalkToMe a
  = Halt
  | Print String
          a
  | Read (String -> a)

instance Functor TalkToMe where
  fmap _ Halt        = Halt
  fmap f (Print s a) = Print s (f a)
  fmap f (Read g)    = Read $ f . g
