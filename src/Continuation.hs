{-# LANGUAGE InstanceSigs #-}

module Continuation where

import Control.Monad
import ExceptDemo

decode c = c []

as f c = c f

a f c = c f

number :: [Integer] -> Integer
number =
  foldr
    (\x y ->
       if x >= y
         then x * y
         else x + y)
    1

one xs c = c $ 1 : xs

two xs c = c $ 2 : xs

three xs c = c $ 3 : xs

seventeen xs c = c $ 17 : xs

twenty xs c = c $ 20 : xs

hundred xs c = c $ 100 : xs

thousand xs c = c $ 1000 : xs

square :: Int -> (Int -> r) -> r
square x c = c $ x ^ 2

add :: Int -> Int -> (Int -> r) -> r
add x y c = c $ x + y

test1 :: Int
test1 = square 3 add 4 add 5 id

t1 :: Integer
t1 = decode one hundred twenty three as a number

t2 :: Integer
t2 = decode one hundred twenty one as a number

t3 :: Integer
t3 = decode one hundred twenty as a number

t4 :: Integer
t4 = decode one hundred as a number

t5 :: Integer
t5 = decode three hundred as a number

t6 :: Integer
t6 = decode two thousand seventeen as a number

sumSquares :: Int -> Int -> (Int -> r) -> r
sumSquares x y c = square x $ \x2 -> square y $ \y2 -> add x2 y2 $ \ss -> c ss

newtype Cont r a = Cont
  { runCont :: (a -> r) -> r
  }

evalCont :: Cont r r -> r
evalCont m = runCont m id

square' :: Int -> Cont r Int
square' x = return $ x ^ 2

add' :: Int -> Int -> Cont r Int
add' x y = return $ x + y

instance Functor (Cont r) where
  fmap = liftM

instance Applicative (Cont r) where
  pure = return
  (<*>) = ap

instance Monad (Cont r) where
  return x = Cont $ \c -> c x
  (Cont v) >>= k = Cont $ \c -> v (\h -> runCont (k h) c)

bind' :: ((a -> r) -> r) -> (a -> (b -> r) -> r) -> (b -> r) -> r
bind' f g br = f (`g` br)

showCont :: Show a => Cont String a -> String
showCont c = runCont c show

-- tryRead :: Read a => String -> Except ReadError a
add'' :: Int -> Int -> FailCont r e Int
add'' x y = FailCont $ \ok _ -> ok $ x + y

addInts :: String -> String -> FailCont r ReadError Int
addInts s1 s2 = do
  i1 <- toFailCont $ tryRead s1
  i2 <- toFailCont $ tryRead s2
  return $ i1 + i2

newtype FailCont r e a = FailCont
  { runFailCont :: (a -> r) -> (e -> r) -> r }

instance Functor (FailCont r e) where
  fmap = liftM

instance Applicative (FailCont r e) where
  pure = return
  (<*>) = ap

instance Monad (FailCont r e) where
  return :: a -> FailCont r e a
  return x = FailCont $ \ok _ -> ok x
  (>>=) :: FailCont r e a -> (a -> FailCont r e b) -> FailCont r e b
  (FailCont fc) >>= h =
    FailCont $ \ok err -> fc (\a' -> runFailCont (h a') ok err) err

toFailCont :: Except e a -> FailCont r e a
toFailCont exc = FailCont $ \ok err -> either err ok (runExcept exc)

evalFailCont :: FailCont (Either e a) e a -> Either e a
evalFailCont fc = runFailCont fc Right Left 

-- test :: Integer -> Cont Integer Integer
-- test x = do
--   a' <- return 3
--   Cont $ \c -> if x > 100 then 42 else c ()
--   return $ a' + x

callCFC :: ((a -> FailCont r e b) -> FailCont r e a) -> FailCont r e a
callCFC f = FailCont $ \ok err -> runFailCont (f $ \a' -> FailCont $ \_ _ -> ok a') ok err

