module BadNaturalTransformation where

type Nat f g a = f a -> g a

-- type Nat f g = forall a. f a -> g a

maybeToList :: Nat Maybe [] a
maybeToList Nothing  = []
maybeToList (Just a) = [a]

-- can change value
degenerateMtl :: Num a => Nat Maybe [] a
degenerateMtl Nothing  = []
degenerateMtl (Just a) = [a + 1]
