module Polymorphism where

import UserTypes

class Printable a where
 toString :: a -> String

instance Printable Bool where
    toString True  = "true"
    toString False = "false"

instance Printable () where
    toString _ = "unit type"

instance (Printable a, Printable b) => Printable (a, b) where
    toString (a, b) = "(" ++ toString a ++ "," ++ toString b ++ ")"


class KnownToGork a where
    stomp :: a -> a
    doesEnrageGork :: a -> Bool

class KnownToMork a where
    stab :: a -> a
    doesEnrageMork :: a -> Bool

class (KnownToGork a, KnownToMork a) => KnownToGorkAndMork a where
    stompOrStab :: a -> a
    stompOrStab a
      | doesEnrageMork a && doesEnrageGork a = stomp $ stab a
      | doesEnrageMork a = stomp a
      | doesEnrageGork a = stab a
      | otherwise = a

-- cyclyc bounded enum
class (Eq a, Bounded a, Enum a) => SafeEnum a where
  ssucc, spred:: a -> a
  ssucc a = if a == maxBound then minBound else succ a
  spred a = if a == minBound then maxBound else pred a

instance SafeEnum Bool

-- orphan instance: this is illegal, you know
instance Orphan Integer where
  next = (+1)

newtype Safe = S Integer
-- that's better
instance Orphan Safe where
  next (S i) = S $ i + 2