module Structures.Exercises where

import Structures.BinarySearchTree

suffixes :: [a] -> [[a]]
suffixes [] = [[]]
suffixes x@(_:xs) = x : suffixes xs

mySet :: Set Int
mySet = (insert 1 . insert 2 . insert 3 . insert 4 . insert 5) empty

myMap :: Map Int Char
myMap =
  let binds = [bind k v | (k, v) <- zip [1 ..] ['a' ..]]
      bigBind = foldr (.) (bind 0 '-') (take 25 binds)
   in bigBind emptyMap
