module BinomialHeap where

-- A binomial tree of rank 0 is a singleton node.
-- A binomial tree of rank r + 1 is formed by linking two binomial trees of 
-- rank r, making one tree the leftmost child of the other.
-- Size of binomial tree of rank r is 2^r
data BinomialTree a = Node
  { rank :: Integer
  , elem :: a
  , children :: [BinomialTree a]
  }

-- only possible to link two trees with same rank
-- Each list of children is maintained in decreasing order of rank, and elements are stored in heap order.
link :: Ord a => BinomialTree a -> BinomialTree a -> BinomialTree a
link t1@(Node r x1 c1) t2@(Node _ x2 c2)
  | x1 <= x2 = Node (r + 1) x1 (t2 : c1)
  | otherwise = Node (r + 1) x2 (t1 : c2)

-- no two trees of the same rank in the list (the order of ranks is increasing)
-- the trees in a binomial heap of size n correspond exactly to the ones in the binary representation of n
type BinomialHeap a = [BinomialTree a]

singleton :: a -> BinomialTree a
singleton a = Node 0 a []

insert :: Ord a => a -> BinomialHeap a -> BinomialHeap a
insert x = insTree (singleton x) where
  insTree t [] = [t]
  insTree t ts@(t':ts') 
    | rank t < rank t' = t : ts
    | otherwise = insTree (link t t') ts'