{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Structures.BinarySearchTree
  ( FiniteSet(empty, insert, member)
  , FiniteMap(emptyMap, bind, lookup)
  , Set
  , Map
  , singleton
  , singletonMap
  , complete
  ) where

import Data.Maybe
import Prelude hiding (elem, lookup)

class FiniteSet s a where
  empty :: s a
  insert :: a -> s a -> s a
  member :: a -> s a -> Bool

-- v and k types are swapped so we can derive instance of FiniteSet
-- for (KVTree ()) from FiniteMap instance by binding unit type as value parameter
class FiniteMap m k where
  emptyMap :: m v k
  bind :: k -> v -> m v k -> m v k
  lookup :: k -> m v k -> Maybe v

data KVTree v k
  = Leaf
  | Node { kvLeft :: KVTree v k
         , kvElem :: (k, v)
         , kvRight :: KVTree v k }
  deriving (Eq, Show)

type Set a = KVTree () a

type Map k v = KVTree v k

singleton :: a -> Set a
singleton e = Node Leaf (e, ()) Leaf

singletonMap :: k -> v -> Map k v
singletonMap k v = Node Leaf (k, v) Leaf

complete :: a -> Int -> Set a
complete = go
  where
    go _ 0 = Leaf
    go x 1 = singleton x
    go x n
      | n < 0 = error "Ты чё, бредишь, что ли?"
      | otherwise =
        let t' = go x (n - 1)
         in Node t' (x, ()) t'

-- unbalanced map with binary tree
instance Ord k => FiniteMap KVTree k where
  emptyMap :: Map k v
  emptyMap = Leaf
  bind :: k -> v -> Map k v -> Map k v
  bind key val Leaf = singletonMap key val
  bind key val node@Node {kvElem = (fstCnd, _)} =
    let tryInsert cndKey Leaf
          | cndKey == key = Nothing
          | otherwise = return (singletonMap key val)
        tryInsert cndKey (Node l entry@(key', _) r)
          | key < key' = updateLeft <$> tryInsert cndKey l
          | otherwise = updateRight <$> tryInsert key' r
          where
            updateLeft l' = Node l' entry r
            updateRight = Node l entry
     in fromMaybe node (tryInsert fstCnd node)
  lookup :: k -> Map k v -> Maybe v
  lookup _ Leaf = Nothing
  lookup key node = member' (kvElem node) node
    where
      member' (cndKey, cndVal) Leaf
        | key == cndKey = Just cndVal
        | otherwise = Nothing
      member' cndEntry (Node l entry@(k', _) r)
        | key < k' = member' cndEntry l
        | otherwise = member' entry r

-- unbalanced set with binary tree
instance Ord a => FiniteSet (KVTree ()) a where
  empty :: Set a
  empty = emptyMap
  insert :: a -> Set a -> Set a
  insert e = bind e ()
  member :: a -> Set a -> Bool
  member k n = isJust (lookup k n)
