{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}

module Structures.Heap where

class AbstractHeap s a where
  empty :: s a
  isEmpty :: s a -> Bool
  insert :: a -> s a -> s a
  merge :: s a -> s a -> s a
  findMin :: s a -> Maybe a
  deleteMin :: s a -> Maybe (s a)

data Heap a
  = E
  | T Int
      a
      (Heap a)
      (Heap a)
  deriving (Eq, Show)

rank :: Heap a -> Int
rank E = 0
rank (T n _ _ _) = n

singleton :: a -> Heap a
singleton x = T 1 x E E

-- leftist heap 
-- ∀ x | parent x != null: elem x >= elem (parent x)
-- ∀ x: rank(left(x)) >= rank(right(x))
instance Ord a => AbstractHeap Heap a where
  empty :: Heap a
  empty = E
  isEmpty :: Heap a -> Bool
  isEmpty E = True
  isEmpty _ = False
  merge :: Heap a -> Heap a -> Heap a -- O(log n)
  merge E h = h
  merge h E = h
  merge h1@(T _ x l1 r1) h2@(T _ y l2 r2)
    | x <= y = makeT x l1 (merge r1 h2)
    | otherwise = makeT y l2 (merge h1 r2)
    where
      makeT e a b
        | rank a >= rank b = T (rank b + 1) e a b
        | otherwise = T (rank a + 1) e b a
  insert :: a -> Heap a -> Heap a -- O(log n)
  insert x = merge (singleton x)
  findMin :: Heap a -> Maybe a -- O(1)
  findMin E = Nothing
  findMin (T _ x _ _) = Just x
  deleteMin :: Heap a -> Maybe (Heap a) -- O(log n)
  deleteMin E = Nothing
  deleteMin (T _ _ a b) = Just (merge a b)

-- ex 3.2
insert' :: Ord a => a -> Heap a -> Heap a
insert' x E = singleton x
insert' x h2@(T _ y l2 r2)
  | x <= y = T 1 x h2 E
  | otherwise = makeT y l2 (insert' x r2)
  where
    makeT e a b
      | rank a >= rank b = T (rank b + 1) e a b
      | otherwise = T (rank a + 1) e b a

fromList :: Ord a => [a] -> Heap a
fromList = mergeAll . map singleton
  where
    mergeAll [] = E
    mergeAll [s] = s
    mergeAll (s1:s2:ss) = s1 `merge` s2 `merge` mergeAll ss

data WeightHeap a
  = Leaf
  | Node Int
         a
         (WeightHeap a)
         (WeightHeap a)
  deriving (Eq, Show)

size :: WeightHeap a -> Int
size Leaf = 0
size (Node s _ _ _) = s

-- Weight-biased leftist heap
instance Ord a => AbstractHeap WeightHeap a where
  empty :: WeightHeap a
  empty = Leaf
  isEmpty :: WeightHeap a -> Bool
  isEmpty Leaf = True
  isEmpty _ = False
  merge :: WeightHeap a -> WeightHeap a -> WeightHeap a
  merge Leaf h = h
  merge h Leaf = h
  merge h1@(Node _ x l1 r1) h2@(Node _ y l2 r2)
    | x <= y = makeT x l1 (merge r1 h2)
    | otherwise = makeT y l2 (merge h1 r2)
    where
      makeT e a b
        | size a >= size b = Node (size b + size a + 1) e a b
        | otherwise = Node (size a + size b + 1) e b a
  insert :: a -> WeightHeap a -> WeightHeap a
  insert x = merge (Node 1 x Leaf Leaf)
  deleteMin :: WeightHeap a -> Maybe (WeightHeap a)
  deleteMin Leaf = Nothing
  deleteMin (Node _ _ a b) = Just (merge a b)
  findMin :: WeightHeap a -> Maybe a
  findMin Leaf = Nothing
  findMin (Node _ x _ _) = Just x