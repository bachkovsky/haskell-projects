module Exercises where

import Data.Char
import Data.List
import Data.List.Split
import Data.Map (fromListWith)
import Numeric

import Control.Monad
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Char (isNumber, isPunctuation)
import Data.Foldable (msum)

-- >>> replaceThe "the cow loves us"
-- "a cow loves us"
replaceThe :: String -> String
replaceThe s' = unwords (map replace (words s'))
  where
    replace s =
      if s == "the"
        then "a"
        else s

-- unfolds
myIterate :: (a -> a) -> a -> [a]
myIterate f a = a : myIterate f (f a)

myUnfoldr :: (b -> Maybe (a, b)) -> b -> [a]
myUnfoldr f z =
  case f z of
    Nothing -> []
    Just (a, b) -> a : myUnfoldr f b

betterIterate :: (a -> a) -> a -> [a]
betterIterate f = myUnfoldr (\e -> Just (e, f e))

data BinaryTree a
  = Leaf
  | Node (BinaryTree a)
         a
         (BinaryTree a)
  deriving (Eq, Ord, Show)

unfold :: (a -> Maybe (a, b, a)) -> a -> BinaryTree b
unfold f z =
  case f z of
    Nothing -> Leaf
    Just (l, b, r) -> Node (unfold f l) b (unfold f r)

treeBuild :: Integer -> BinaryTree Integer
treeBuild n =
  unfold
    (\i ->
       if i == 0
         then Nothing
         else Just (i - 1, n - i, i - 1))
    n

fibonacci :: Int -> Integer
fibonacci n
  | n < 0 =
    (if n `mod` 2 == 0
       then (-1)
       else 1) *
    fibonacci (-n)
  | otherwise = fibs !! n

fibs :: [Integer]
fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

seqA :: Int -> Integer
seqA n = generate [1, 2, 3] n !! n

next :: [Integer] -> Integer
next xs = xs !! (l - 1) + xs !! (l - 2) - 2 * xs !! (l - 3)
  where
    l = length xs

generate :: [Integer] -> Int -> [Integer]
generate xs n
  | length xs > n = xs
  | otherwise = xss ++ [next xss]
  where
    xss = generate xs (n - 1)

sumNCount :: Integer -> (Int, Int)
sumNCount i = (sum l, length l)
  where
    l = map digitToInt $ filter isDigit $ show i

integration :: (Double -> Double) -> Double -> Double -> Double
integration f a b = delta * (f a + f b) / 2 + sum (map (f . seg) [2 .. n - 1])
  where
    n = 5000
    delta = (b - a) / n
    seg i = a + i * delta

getSecondFrom :: a -> b -> c -> b
getSecondFrom _ y _ = y

on3 :: (b -> b -> b -> c) -> (a -> b) -> a -> a -> a -> c
on3 op f x y z = op (f x) (f y) (f z)

swap :: (b, a) -> (a, b)
swap = uncurry (flip (,))

-- swap (1, "AS")
ip :: String
ip = show aaa ++ show bbb ++ show ccc ++ show ddd

aaa :: Integer
aaa = 12

bbb :: Double
bbb = 7.22

ccc :: Double
ccc = 4.12

ddd :: Double
ddd = 0.12

avg :: Int -> Int -> Int -> Double
avg a b c = fromInteger (toInteger a + toInteger b + toInteger c) / 3.0

accum :: String -> String
accum s =
  intercalate
    "-"
    [toUpper ch : replicate i (toLower ch) | (i, ch) <- zip [0 ..] s]

seriesSum :: Integer -> String
seriesSum n = showFFloat (Just 2) num ""
  where
    num = sum . map (1 /) . genericTake n $ [1,4 ..]

anagrams :: String -> [String] -> [String]
anagrams w ws = [word | word <- ws, anagram word == anagram w]
  where
    anagram s = fromListWith (+) $ zip s [1,1 ..]

solution :: String -> [String]
solution s
  | even (length s) = chunksOf 2 s
  | otherwise = chunksOf 2 (s ++ "_")

findMissing :: Integral n => [n] -> n
findMissing xs@(f:s:t:_) = findM xs step
  where
    step = signum (s - f) * minimum (map abs [s - f, t - s])
    findM xs'@(f':s':_) n
      | f' + n == s' = findM (tail xs') n
      | otherwise = f' + n
    findM _ _ = error "Sosay"
findMissing _ = error "Sasay"

xxx :: Integer
xxx = findMissing (1 : 3 : [7,9 ..])

askPassword0 :: MaybeT IO ()
askPassword0 = do
  liftIO $ putStrLn "Enter your new password:"
  value <- msum $ repeat getValidPassword0
  liftIO $ putStrLn "Storing in database..."

getValidPassword0 :: MaybeT IO String
getValidPassword0 = do
  s <- liftIO getLine
  guard (isValid0 s)
  return s

isValid0 :: String -> Bool
isValid0 s = length s >= 8 && any isNumber s && any isPunctuation s

newtype PwdError =
  PwdError String
  deriving (Eq, Show)

type PwdErrorIOMonad = ExceptT PwdError IO

instance Semigroup PwdError where
  (PwdError s1) <> (PwdError s2) = PwdError (s1 <> s2)

instance Monoid PwdError where
  mempty = PwdError ""

askPassword :: PwdErrorIOMonad ()
askPassword = do
  liftIO $ putStrLn "Enter your new password:"
  _ <- msum $ repeat getValidPassword
  liftIO $ putStrLn "Storing in database..."

getValidPassword :: PwdErrorIOMonad String
getValidPassword = do
  input <- liftIO getLine
  unless
    (length input >= 8)
    (show'n'throw "Incorrect input: password is too short!")
  unless
    (any isNumber input)
    (show'n'throw "Incorrect input: password must contain some digits!")
  unless
    (any isPunctuation input)
    (show'n'throw "Incorrect input: password must contain some punctuation!")
  return input
  where
    show'n'throw msg = liftIO (putStrLn msg) >> throwE (PwdError msg)