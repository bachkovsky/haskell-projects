{-# LANGUAGE TupleSections #-}

module Monoids where

import qualified Data.List   as L
import           Data.Monoid ((<>))
import           Prelude     hiding (lookup)

class Monoid' a where
  mempty' :: a -- нейтральный элемент
  mappend' :: a -> a -> a -- операция
  mconcat' :: [a] -> a -- свертка
  mconcat' = foldr mappend' mempty'

{-законы:
[левый нейтральный элемент]  mempty `mappend` x === x
[правый нейтральный элемент] x `mappend` mempty === x
[ассоциативность]            (x `mappend` y) `mappend` z === x `mappend` ( y `mappend` z)
-}
instance Monoid' [a] where
  mempty' = []
  mappend' = (++)

newtype Sum' a = Sum'
  { getSum :: a
  } deriving (Eq, Ord, Read, Show, Bounded)

instance Num a => Monoid' (Sum' a) where
  mempty' = Sum' 0
  Sum' x `mappend'` Sum' y = Sum' (x + y)

newtype Product' a = Product'
  { getProduct :: a   
  } deriving (Eq, Ord, Read, Show, Bounded)

instance Num a => Monoid' (Product' a) where
  mempty' = Product' 1
  Product' x `mappend'` Product' y = Product' (x * y)

newtype Xor = Xor
  { getXor :: Bool
  } deriving (Eq, Show)

instance Semigroup Xor where
  Xor x <> Xor y = Xor $ x /= y

instance Monoid Xor where
  mempty = Xor False

instance (Monoid' a, Monoid' b) => Monoid' (a, b) where
  mempty' = (mempty', mempty')
  (x1, x2) `mappend'` (y1, y2) = (x1 `mappend'` y1, x2 `mappend'` y2)

instance Monoid' a => Monoid' (Maybe a) where
  mempty' = Nothing
  Nothing `mappend'` a = a
  a `mappend'` Nothing = a
  Just a `mappend'` Just b = Just $ a `mappend'` b

newtype First' a = First'
  { getFirst :: Maybe a
  } deriving (Eq, Ord, Read, Show)

instance Semigroup (First' a) where
  First' Nothing <> r = r
  l <> _ = l

-- свертка всегда возвращает первый непустой элемент:
-- *Monoids> mconcat . map First' $  [Nothing, Nothing, Just 3, Just 5]
--   First' {getFirst = Just 3}
instance Monoid (First' a) where
  mempty = First' Nothing

newtype Maybe' a = Maybe'
  { getMaybe :: Maybe a
  } deriving (Eq, Show)

instance Semigroup a => Semigroup (Maybe' a) where
  _ <> n@(Maybe' Nothing) = n
  n@(Maybe' Nothing) <> _ = n
  Maybe' l <> Maybe' r = Maybe' $ l <> r

instance Monoid a => Monoid (Maybe' a) where
  mempty = Maybe' (Just mempty)

test0 :: String
test0 =
  if (mempty :: Maybe' [Int]) == Maybe' Nothing
    then "failed"
    else "passed"

newtype ListMap k v = ListMap
  { getListMap :: [(k, v)]
  } deriving (Eq, Show)

instance MapLike ListMap where
  empty = ListMap []
  lookup k (ListMap m) = L.lookup k m
  insert key val m = ListMap $ (key, val) : getListMap (delete key m)
  delete key (ListMap m) = ListMap $ filter (\(k, _) -> k /= key) m

class MapLike m where
  empty :: m k v
  lookup :: Ord k => k -> m k v -> Maybe v
  insert :: Ord k => k -> v -> m k v -> m k v
  delete :: Ord k => k -> m k v -> m k v
  fromList :: Ord k => [(k, v)] -> m k v
  fromList []          = empty
  fromList ((k, v):xs) = insert k v (fromList xs)

newtype ArrowMap k v = ArrowMap
  { getArrowMap :: k -> Maybe v
  }

instance MapLike ArrowMap where
  empty = ArrowMap $ const Nothing
  lookup x (ArrowMap f) = f x
  insert key val (ArrowMap f) =
    ArrowMap $ \x ->
      if x == key
        then Just val
        else f x
  delete key (ArrowMap f) =
    ArrowMap $ \x ->
      if x == key
        then Nothing
        else f x

newtype Mem s a = Mem
  { runMem :: s -> (a, s)
  }

instance Semigroup a => Semigroup (Mem s a) where
  (Mem f1) <> (Mem f2) =
    Mem $ \s ->
      let (a', s') = f1 s
          (a'', s'') = f2 s'
       in (a' <> a'', s'')

instance Monoid a => Monoid (Mem s a) where
  mempty = Mem (mempty, )

f' :: Mem Integer String
f' = Mem $ \s -> ("hi", s + 1)

main :: IO ()
main = do
  print $ runMem (f' <> mempty) 0
  print $ runMem (mempty <> f') 0
  print (runMem mempty 0 :: (String, Int))
  print $ runMem (f' <> mempty) 0 == runMem f' 0
  print $ runMem (mempty <> f') 0 == runMem f' 0
