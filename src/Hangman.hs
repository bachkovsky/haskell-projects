{-# LANGUAGE NamedFieldPuns #-}

module Hangman where

import Control.Monad (forever, when)
import Data.Char (toLower)
import Data.List (intersperse)
import Data.Maybe (fromMaybe, isJust)
import System.Exit (exitSuccess)
import System.IO (BufferMode(NoBuffering), hSetBuffering, stdout)
import System.Random (randomRIO)

newtype WordList =
  WordList [String]
  deriving (Eq, Show)

readWords :: IO WordList
readWords = do
  dict <- readFile "/usr/share/dict/words"
  return $ WordList (lines dict)

minWordLength :: Int
minWordLength = 5

maxWordLength :: Int
maxWordLength = 20

gameWords :: IO WordList
gameWords = do
  WordList allWords <- readWords
  return $ WordList (filter gameLength allWords)
  where
    gameLength w = length w > minWordLength && length w < maxWordLength

randomWord :: WordList -> IO String
randomWord (WordList ws) = do
  randomIndex <- randomRIO (0, length ws - 1)
  return $ ws !! randomIndex

randomWord' :: IO String
randomWord' = gameWords >>= randomWord

type GuessWord = String

type FilledChars = [Maybe Char]

type GuessedChars = String

type Errors = Int

data Puzzle = Puzzle
  { word :: GuessWord
  , filled :: FilledChars
  , guessed :: GuessedChars
  , errors :: Errors
  } deriving (Eq)

instance Show Puzzle where
  show Puzzle {filled, guessed, errors} =
    intersperse ' ' (fmap (fromMaybe '_') filled) ++
    " Guessed so far: " ++
    guessed ++ "\nErrors left: " ++ show (maxErrors - errors)

freshPuzzle :: String -> Puzzle
freshPuzzle word = Puzzle word (replicate (length word) Nothing) [] 0

charInWord :: Puzzle -> Char -> Bool
charInWord Puzzle {word} c = c `elem` word

alreadyGuessed :: Puzzle -> Char -> Bool
alreadyGuessed Puzzle {guessed} c = c `elem` guessed

fillInCharacter :: Puzzle -> Char -> Puzzle
fillInCharacter puzzle guess =
  puzzle {filled = newFilledIn, guessed = guess : guessed puzzle}
  where
    newFilledIn = zipWith zipper (word puzzle) (filled puzzle)
    zipper c g
      | guess == c = Just c
      | otherwise = g

handleGuess :: Puzzle -> Char -> IO Puzzle
handleGuess puzzle guess = do
  putStrLn $ "Your guess was: " ++ [guess]
  case (charInWord puzzle guess, alreadyGuessed puzzle guess) of
    (_, True) -> do
      putStrLn "You already guessed that character, pick something else!"
      return puzzle
    (True, _) -> do
      putStrLn "This character was in the word, filling in the word accordingly"
      return $ fillInCharacter puzzle guess
    (False, _) -> do
      putStrLn "This character wasn't in the word, try again."
      let puzzle' = fillInCharacter puzzle guess
      return $ puzzle' {errors = errors puzzle' + 1}

maxErrors :: Int
maxErrors = 5

checkGameOver :: Puzzle -> IO ()
checkGameOver Puzzle {word, errors} =
  when (errors >= maxErrors) $ do
    putStrLn "You lose!"
    putStrLn ("The word was: " ++ word)
    exitSuccess

checkGameWin :: Puzzle -> IO ()
checkGameWin Puzzle {word, filled} =
  when (all isJust filled) $ do
    putStrLn "You win!"
    putStrLn ("The word was: " ++ word)
    exitSuccess

runGame :: Puzzle -> IO ()
runGame puzzle = hSetBuffering stdout NoBuffering >> forever runRound
  where
    runRound = do
      checkGameOver puzzle
      checkGameWin puzzle
      putStrLn $ "Current puzzle is: " ++ show puzzle
      putStr "Guess a letter: "
      guess <- getLine
      case guess of
        [c] -> handleGuess puzzle c >>= runGame
        _ -> putStrLn "Your guess must be a single character"

runHangman :: IO ()
runHangman = do
  word <- randomWord'
  let puzzle = freshPuzzle (fmap toLower word)
  runGame puzzle
