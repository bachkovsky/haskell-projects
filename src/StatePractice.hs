{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE TupleSections #-}

module StatePractice where

import Control.Applicative (liftA3)
import Control.Monad (replicateM)
import Control.Monad.Trans.Class
import Control.Monad.Trans.State
import Control.Monad.Trans.Writer
import Data.Monoid
import System.Random

data Die
  = DieOne
  | DieTwo
  | DieThree
  | DieFour
  | DieFive
  | DieSix
  deriving (Eq, Show)

type RandomS a = State StdGen a

intToDie :: Int -> Die
intToDie 1 = DieOne
intToDie 2 = DieTwo
intToDie 3 = DieThree
intToDie 4 = DieFour
intToDie 5 = DieFive
intToDie 6 = DieSix
intToDie x = error $ "Int not in range " ++ show x

rollDie :: RandomS Die
rollDie = intToDie <$> state (randomR (1, 6))

rollThree :: RandomS (Die, Die, Die)
rollThree = liftA3 (,,) rollDie rollDie rollDie

nDie :: Int -> RandomS [Die]
nDie n = replicateM n rollDie

rollsToGetN :: Int -> StdGen -> (Int, [Die])
rollsToGetN limit = go 0 []
  where
    go :: Int -> [Die] -> StdGen -> (Int, [Die])
    go amount dies gen
      | amount >= limit = (length dies, dies)
      | otherwise =
        let (die, nextGen) = randomR (1, 6) gen
         in go (amount + die) (intToDie die : dies) nextGen

newtype Moi s a = Moi
  { runMoi :: s -> (a, s)
  }

instance Functor (Moi s) where
  fmap :: (a -> b) -> Moi s a -> Moi s b
  fmap f (Moi g) =
    Moi $ \s ->
      let (a, s') = g s
       in (f a, s')

instance Applicative (Moi s) where
  pure :: a -> Moi s a
  pure a = Moi (a, )
  -- s -> (a -> b, s)
  -- Moi s a -> b
  -- Moi s (a -> b)
  (<*>) :: Moi s (a -> b) -> Moi s a -> Moi s b
  (Moi f) <*> (Moi g) =
    Moi $ \s ->
      let (fab, s') = f s
          (a, s'') = g s'
       in (fab a, s'')

instance Monad (Moi s) where
  return = pure
  (>>=) :: Moi s a -> (a -> Moi s b) -> Moi s b
  (Moi f) >>= g =
    Moi $ \s ->
      let (a, s') = f s
          (Moi h) = g a
       in h s'

data Tree a
  = Leaf a
  | Fork (Tree a)
         a
         (Tree a)
  deriving (Eq, Show)

numberAndCount :: Tree () -> (Tree Integer, Integer)
numberAndCount t = getSum <$> runWriter (evalStateT (go t) 1)

go :: Tree () -> StateT Integer (Writer (Sum Integer)) (Tree Integer)
go (Leaf _) = do
  x <- get
  put (x + 1)
  lift $ tell (Sum 1)
  return (Leaf x)
go (Fork l _ r) = do
  ll <- go l
  x <- get
  put (x + 1)
  rr <- go r
  return (Fork ll x rr)
