{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE InstanceSigs #-}

module MTL.WriterClass where

import Transformers.MonadTrans
import Transformers.StateT
import qualified Transformers.WriterT as W

class (Monoid w, Monad m) =>
      MonadWriter w m
  | m -> w
  where
  writer :: (a, w) -> m a
  tell :: w -> m ()
  listen :: m a -> m (a, w)

listens :: MonadWriter w m => (w -> w') -> m a -> m (a, w')
listens f m = do
  ~(a, w) <- listen m
  return (a, f w)

instance (Monoid w, Monad m) => MonadWriter w (W.WriterT w m) where
  writer = W.writer
  tell = W.tell
  listen = W.listen

instance MonadWriter w m => MonadWriter w (StateT s m) where
  writer = lift . writer
  tell = lift . tell
  listen m =
    StateT $ \s -> do
      ~((a, s'), w) <- listen (runStateT m s)
      return ((a, w), s')
