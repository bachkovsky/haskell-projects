module MTL.Writer
  ( MonadWriter(..)
  , listens
  , WriterT(WriterT)
  , runWriterT
  , execWriterT
  ) where

import MTL.WriterClass
import Transformers.MonadTrans
import Transformers.WriterT (WriterT(WriterT), execWriterT, runWriterT)
