{-# LANGUAGE FlexibleContexts #-}

module MTL.Exercises where

import Control.Arrow
import Control.Monad.Except
import Control.Monad.Identity
import Control.Monad.Writer
import Data.Foldable
import Text.Read

data ReadError
  = EmptyInput
  | NoParse String
  deriving (Show)

tryRead :: (Read a, MonadError ReadError m) => String -> m a
tryRead "" = throwError EmptyInput
tryRead s = liftEither $ left NoParse $ readEither s

data Tree a
  = Leaf a
  | Fork (Tree a)
         a
         (Tree a)

treeSum :: Tree String -> Either ReadError Integer
treeSum (Leaf a) = tryRead a
treeSum (Fork l a r) = do
    lx <- treeSum l
    x <- tryRead a
    rx <- treeSum r
    return (lx + x + rx)