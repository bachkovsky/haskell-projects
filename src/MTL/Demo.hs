module MTL.Demo where

import Control.Monad.Identity
import MTL.Writer
import Transformers.StateT

type SiWs = StateT Integer (WriterT String Identity)

test :: SiWs ()
test = do
  x <- get
  when (x > 100) (tell "Overflow!") -- tell without lift
  put 42

runSiWs :: SiWs a -> Integer -> ((a, Integer), String)
runSiWs m = runIdentity . runWriterT . runStateT m