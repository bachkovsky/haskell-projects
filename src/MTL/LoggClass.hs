{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE InstanceSigs #-}

module MTL.LoggClass where

import Control.Monad.Reader
import Control.Monad.State
import Data.Functor.Identity
import Transformers.LoggT

instance MonadTrans LoggT where
  lift :: Monad m => m a -> LoggT m a
  lift m = LoggT (Logged "" <$> m)

instance MonadState s m => MonadState s (LoggT m) where
  get = lift get
  put = lift . put
  state = lift . state

logSt' :: LoggT (State Integer) Integer
logSt' = do
  modify (+ 1) -- no lift!
  a <- get -- no lift!
  write2log $ show $ a * 10
  put 42 -- no lift!
  return $ a * 100

instance MonadReader r m => MonadReader r (LoggT m) where
  ask = lift ask
  local = mapLoggT . local
  reader = lift . reader

mapLoggT :: (m (Logged a) -> n (Logged b)) -> LoggT m a -> LoggT n b
mapLoggT f l = LoggT $ f (runLoggT l)

logRdr :: LoggT (Reader [(Int, String)]) ()
logRdr = do
  Just x <- asks $ lookup 2 -- no lift!
  write2log x
  Just y <- local ((3, "Jim") :) $ asks $ lookup 3 -- no lift!
  write2log y

class Monad m => MonadLogg m where
  w2log :: String -> m ()
  logg :: Logged a -> m a

instance Monad m => MonadLogg (LoggT m) where
  w2log = write2log
  logg  = LoggT . return

logSt'' :: LoggT (State Integer) Integer      
logSt'' = do 
  x <- logg $ Logged "BEGIN " 1
  modify (+x)
  a <- get
  w2log $ show $ a * 10
  put 42
  w2log " END"
  return $ a * 100

instance MonadLogg m => MonadLogg (StateT s m) where
  w2log = lift . w2log
  logg  = lift . logg

instance MonadLogg m => MonadLogg (ReaderT r m) where
  w2log = lift . w2log
  logg  = lift . logg

rdrStLog :: ReaderT Integer (StateT Integer Logg) Integer      
rdrStLog = do 
  x <- logg $ Logged "BEGIN " 1
  y <- ask
  modify (+ (x+y))
  a <- get
  w2log $ show $ a * 10
  put 42
  w2log " END"
  return $ a * 100