{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module MTL.MultiParamDemo where

infixl 7 ***

class Mult a b c | a b -> c where
  (***) :: a -> b -> c

instance Mult Int Int Int where
  (***) = (*)
  -- (***) = (*)

-- functional dependency violation
-- instance Mult Int Int Double where
instance Mult Double Double Double where
  (***) = (*)

instance Mult Int Double Double where
  i *** d = fromIntegral i * d

instance Mult Double Int Double where
  d *** i = i *** d

imposibru :: Int -> Double -> Double
-- imposibru x y = x * y 
imposibru x y = x *** y

class Functor' c e | c -> e where
  fmap' :: (e -> e) -> c -> c

instance Functor' [a] a where
  fmap' _ [] = []
  fmap' f (x:xs) = f x : fmap' f xs

instance Functor' (Maybe a) a where
  fmap' _ Nothing = Nothing
  fmap' f (Just x) = Just (f x)
