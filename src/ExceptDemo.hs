module ExceptDemo where

import Control.Applicative
import Control.Arrow
import Control.Monad
import Text.Read

newtype Except e a = Except
  { runExcept :: Either e a
  } deriving (Show)

except :: Either e a -> Except e a
except = Except

instance Functor (Except e) where
  fmap = liftM

instance Applicative (Except e) where
  pure = return
  (<*>) = ap

instance Monad (Except e) where
  return = except . Right
  (Except e) >>= f = except (e >>= runExcept . f)

withExcept :: (e -> e') -> Except e a -> Except e' a
withExcept f (Except l) = except (left f l)

throwE :: e -> Except e a
throwE = except . Left

catchE :: Except e a -> (e -> Except e' a) -> Except e' a
m `catchE` h =
  case runExcept m of
    Left e -> h e
    Right r -> except (Right r)

{-
Usage: 
  do {action1; action2; action3 } `catchE` handler
Law:
  (throwE e) `catchE` h == h e
-}
data DivByError
  = ErrZero String
  | ErrOther
  deriving (Eq, Show)

(/?) :: Double -> Double -> Except DivByError Double
x /? 0 = throwE $ ErrZero (show x ++ "/0")
x /? y = return $ x / y

example0 :: Double -> Double -> Except DivByError String
example0 x y = action `catchE` handler
  where
    action = do
      q <- x /? y
      return $ show q
    handler :: DivByError -> Except DivByError String
    handler = return . show

data ListIndexError
  = ErrIndexTooLarge Int
  | ErrNegativeIndex
  deriving (Eq, Show)

infixl 9 !!!

(!!!) :: [a] -> Int -> Except ListIndexError a
(!!!) xs i
  | i < 0 = throwE ErrNegativeIndex
  | otherwise = scan xs i
  where
    scan (x:_) 0 = return x
    scan [] _ = throwE $ ErrIndexTooLarge i
    scan (_:es) idx = scan es (idx - 1)

data ReadError
  = EmptyInput
  | NoParse String
  deriving (Show)

tryRead :: Read a => String -> Except ReadError a
tryRead "" = throwE EmptyInput
tryRead s = except . left NoParse . readEither $ s

data SumError =
  SumError Int
           ReadError
  deriving (Show)

trySum :: [String] -> Except SumError Integer
trySum ss =
  sum <$>
  sequenceA [withExcept (SumError i) (tryRead s) | (i, s) <- zip [1 ..] ss]

instance Monoid e => Alternative (Except e) where
  empty = mzero
  (<|>) = mplus

instance Monoid e => MonadPlus (Except e) where
  mzero = except (Left mempty)
  Except x `mplus` Except y =
    except $
    case x of
      Left e -> either (Left . mappend e) Right y
      r -> r

instance Semigroup DivByError where
  ErrZero s1 <> ErrZero s2 = ErrZero $ s1 ++ s2
  e <> ErrOther = e
  ErrOther <> e = e

instance Monoid DivByError where
  mempty = ErrOther

example2 :: Double -> Double -> Except DivByError String
example2 x y = try `catchE` catch
  where
    try = do
      q <- x /? y
      guard $ y >= 0
      return $ show q
    catch (ErrZero s) = return s
    catch ErrOther = return "NonNegative Guard"

newtype SimpleError = Simple
  { getSimple :: String
  } deriving (Eq, Show)

instance Semigroup SimpleError where
  Simple s1 <> Simple s2 = Simple $ s1 ++ s2

instance Monoid SimpleError where
  mempty = Simple ""

lie2se :: ListIndexError -> SimpleError
lie2se (ErrIndexTooLarge i) = Simple $ "[index (" ++ show i ++ ") is too large]"
lie2se ErrNegativeIndex = Simple "[negative index]"

newtype Validate e a = Validate { getValidate :: Either [e] a } deriving (Eq, Show)

collectE :: Except e a -> Validate e a
collectE = Validate . left (: []) . runExcept

instance Functor (Validate e) where
  fmap f (Validate e) = Validate $ f <$> e

instance Applicative (Validate e) where
  pure = Validate . Right
  Validate f <*> Validate a = Validate $ case (f, a) of
    (Left e1, Left e2) -> Left $ e1 ++ e2
    (Right h, Right x) -> Right $ h x
    (Left e, _) -> Left e
    (_, Left e) -> Left e

validateSum :: [String] -> Validate SumError Integer
validateSum = fmap sum . traverse (\(i, s) -> collectE $ withExcept (SumError i) (tryRead s)) . zip [1..]