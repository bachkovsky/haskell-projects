{-# LANGUAGE InstanceSigs #-}

module Queue
  ( IQueue
  , Queue
  , SQueue
  , push
  , pop
  , empty
  , isEmpty
  ) where

class IQueue q where
  push :: a -> q a -> q a
  pop :: q a -> Maybe (a, q a)
  empty :: q a

data Queue a = Q
  { enqueue :: [a]
  , dequeue :: [a]
  } deriving (Eq, Show)

instance IQueue Queue where
  push :: a -> Queue a -> Queue a
  push e (Q es ds) = Q (e : es) (ds ++ [e])
  pop :: Queue a -> Maybe (a, Queue a)
  pop (Q [] []) = Nothing
  pop (Q enq (d:ds)) =
    let enq' =
          (if null enq
             then []
             else init enq)
     in Just (d, Q enq' ds)
  pop q@(Q _ []) = pop (rebalance q)
  empty :: Queue a
  empty = Q [] []

isEmpty :: (Eq (q a), IQueue q) => q a -> Bool
isEmpty = (== empty)

newtype SQueue a = SQ
  { unqueue :: [a]
  } deriving (Eq, Show)

instance IQueue SQueue where
  push :: a -> SQueue a -> SQueue a
  push e (SQ es) = SQ (e : es)
  pop :: SQueue a -> Maybe (a, SQueue a)
  pop (SQ []) = Nothing
  pop (SQ xs) =
    let (i, l) = initLast' xs
     in Just (l, SQ i)
    where
      initLast' :: [a] -> ([a], a)
      initLast' [e] = ([], e)
      initLast' (e:es) =
        let (es', y) = initLast' es
         in (e : es', y)
  empty :: SQueue a
  empty = SQ []

rebalance :: Queue a -> Queue a
rebalance (Q es []) = Q es (reverse es)
rebalance (Q [] ds) = Q (reverse ds) ds
rebalance queue = queue
