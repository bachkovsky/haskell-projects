module Tree where

import           Data.Char

data Tree a
  = Leaf a
  | Fork (Tree a)
         a
         (Tree a)
  deriving (Show)

numberTree :: Tree () -> Tree Integer
numberTree tree = fst $ numberTree' tree 0

numberTree' :: Tree () -> Integer -> (Tree Integer, Integer)
numberTree' (Leaf _) n = (Leaf n', n')
  where
    n' = n + 1
numberTree' (Fork left _ right) n = (Fork left' (n' + 1) right', n'')
  where
    (left', n') = numberTree' left n
    (right', n'') = numberTree' right (n' + 1)

data BinaryTree a
  = BLeaf
  | BNode (BinaryTree a)
          a
          (BinaryTree a)
  deriving (Eq, Ord, Show)

insert' :: Ord a => a -> BinaryTree a -> BinaryTree a
insert' e BLeaf = BNode BLeaf e BLeaf
insert' e tree@(BNode l e' r)
  | e < e' = BNode (insert' e l) e' r
  | e > e' = BNode l e' (insert' e r)
  | otherwise = tree

infixr 5 =:

(=:) :: a -> (BinaryTree a, BinaryTree a) -> BinaryTree a
x =: (l, r) = BNode l x r

instance Functor BinaryTree where
  fmap _ BLeaf         = BLeaf
  fmap f (BNode l e r) = BNode (fmap f l) (f e) (fmap f r)

testTree' :: BinaryTree Integer
testTree' = BNode (BNode BLeaf 3 BLeaf) 1 (BNode BLeaf 4 BLeaf)

mapExpected :: BinaryTree Integer
mapExpected = BNode (BNode BLeaf 4 BLeaf) 2 (BNode BLeaf 5 BLeaf)

-- acceptance test for mapTree
mapOkay :: IO ()
mapOkay =
  if fmap (+ 1) testTree' == mapExpected
    then print "yup okay!"
    else error "test failed!"

preorder :: BinaryTree a -> [a]
preorder BLeaf         = []
preorder (BNode l e r) = e : preorder l ++ preorder r

inorder :: BinaryTree a -> [a]
inorder BLeaf         = []
inorder (BNode l e r) = inorder l ++ [e] ++ inorder r

postorder :: BinaryTree a -> [a]
postorder BLeaf         = []
postorder (BNode l e r) = postorder l ++ postorder r ++ [e]

testTree :: BinaryTree Integer
testTree = BNode (BNode BLeaf 1 BLeaf) 2 (BNode BLeaf 3 BLeaf)

testPreorder :: IO ()
testPreorder =
  if preorder testTree == [2, 1, 3]
    then putStrLn "Preorder fine!"
    else putStrLn "Bad news bears."

testInorder :: IO ()
testInorder =
  if inorder testTree == [1, 2, 3]
    then putStrLn "Inorder fine!"
    else putStrLn "Bad news bears."

testPostorder :: IO ()
testPostorder =
  if postorder testTree == [1, 3, 2]
    then putStrLn "Postorder fine!"
    else putStrLn "postorder failed check"

main :: IO ()
main = do
  testPreorder
  testInorder
  testPostorder

-- preorder fold
instance Foldable BinaryTree where
  foldr _ z BLeaf         = z
  foldr f z (BNode l x r) = f x (foldr f (foldr f z r) l)