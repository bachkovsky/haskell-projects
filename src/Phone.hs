module Phone where

import Data.Char (isUpper, toUpper)
import Data.List (elemIndex, find)

newtype DaPhone =
  DaPhone [Button]

type Button = (Digit, String)

phoneKeyboard :: DaPhone
phoneKeyboard =
  DaPhone
    [ ('1', "1")
    , ('2', "ABC2")
    , ('3', "DEF3")
    , ('4', "GHI4")
    , ('5', "JKL5")
    , ('6', "MNO6")
    , ('7', "PQRS7")
    , ('8', "TUV8")
    , ('9', "WXYZ9")
    , ('*', "*")
    , ('0', " 0")
    , ('#', "#.,")
    ]

convo :: [String]
convo =
  [ "Wanna play 20 questions"
  , "Ya"
  , "U 1st haha"
  , "Lol ok. Have u ever tasted alcohol lol"
  , "Lol ya"
  , "Wow ur cool haha. Ur turn"
  , "Ok. Do u think I am pretty Lol"
  , "Lol ya"
  , "Haha thanks just making sure rofl ur turn"
  ]

-- validButtons = "1234567890*#"
type Digit = Char

-- Valid presses: 1 and up
type Presses = Int

reverseTaps :: DaPhone -> Char -> [(Digit, Presses)]
reverseTaps phone c = tapsFor (charCoord phone c)
  where
    tapsFor Nothing = []
    tapsFor (Just tapSeq) =
      if isUpper c
        then [('*', 1), tapSeq]
        else [tapSeq]

-- return char coordinates in phone keyboard -
-- digit and number of taps, or Nothing if char is not in keyboard
charCoord :: DaPhone -> Char -> Maybe (Digit, Presses)
charCoord phone c = do
  (dig, str) <- findButton phone c
  idx <- elemIndex (toUpper c) str
  return (dig, idx)

findButton :: DaPhone -> Char -> Maybe Button
findButton (DaPhone keyboard) ch = find (buttonFor ch) keyboard
  where
    ch' `buttonFor` (_, str) = toUpper ch' `elem` str

-- skips symbols of string that are not present in keyboard
cellPhonesDead :: DaPhone -> String -> [(Digit, Presses)]
cellPhonesDead phone string =
  let taps = [reverseTaps phone c | c <- string]
   in if any null taps
        then []
        else concat taps

fingerTaps :: [(Digit, Presses)] -> Presses
fingerTaps = sum . map snd