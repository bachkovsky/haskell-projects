module QueueSpec where

import qualified Queue as Q
import Test.Hspec

test :: IO ()
test =
  hspec $ do
    describe
      "Queue"
      (testQueue
         (Q.empty :: Q.Queue Int)
         (beforePop :: Q.Queue Int)
         (afterPop :: Q.Queue Int))
    describe
      "Single List Queue"
      (testQueue
         (Q.empty :: Q.SQueue Int)
         (beforePop :: Q.SQueue Int)
         (afterPop :: Q.SQueue Int))
  where
    beforePop :: Q.IQueue q => q Int
    beforePop = foldr Q.push Q.empty [1 .. 100]
    afterPop :: Q.IQueue q => q Int
    afterPop = foldr Q.push Q.empty [1 .. 99]

testQueue ::
     (Eq (q Int), Show (q Int), Q.IQueue q)
  => q Int
  -> q Int
  -> q Int
  -> SpecWith ()
testQueue emptyQueue beforePop afterPop = do
  it "Creates empty queue from empty list" $
    Q.isEmpty emptyQueue `shouldBe` True
  it "Returns Nothing when calling pop on empty queue" $
    Q.pop emptyQueue `shouldBe` Nothing
  it "Returns first pushed element when calling pop on non-empty queue" $ do
    Q.pop (Q.push 1 emptyQueue) `shouldBe` Just (1, emptyQueue)
    Q.pop (Q.push 2 (Q.push 1 emptyQueue)) `shouldBe`
      Just (1, Q.push 2 emptyQueue)
    Q.pop beforePop `shouldBe` Just (100, afterPop)
  it "Returns singleton queue when push single element into empty queue" $
    Q.push 1 emptyQueue `shouldBe` Q.push 1 emptyQueue
