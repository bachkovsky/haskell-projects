module FunctorSpec where

import qualified Applicative as A
import Functors
import Test.Hspec
import Test.Hspec.Checkers
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes
import Control.Applicative

functorIdentity :: (Functor f, Eq (f a)) => f a -> Bool
functorIdentity f = fmap id f == f

functorCompose :: (Functor f, Eq (f c)) => (a -> b) -> (b -> c) -> f a -> Bool
functorCompose g f functor = fmap (f . g) functor == (fmap f . fmap g) functor

functorCompose' :: (Eq (f c), Functor f) => f a -> Fun a b -> Fun b c -> Bool
functorCompose' x (Fun _ f) (Fun _ g) = fmap (g . f) x == (fmap g . fmap f $ x)

test :: IO ()
test =
  hspec $ do
    describe "Functor Two" $ do
      it "Obeys identity law" $
        property $ \x -> functorIdentity (x :: (Two Char Int))
      it "Obeys composotion law" $
        property $ \x -> functorCompose (+ 1) (* 2) (x :: (Two Char Int))
    describe "Functor Or" $ do
      it "Obeys identity law" $
        property $ \x -> functorIdentity (x :: (Or Char Int))
      it "Obeys composotion law" $
        property $ \x -> functorCompose (+ 1) (* 2) (x :: (Or Char Int))
    describe "checkers :: testBatch" $ do
      it "allows to test the monoid laws" $ do
        monoid' (undefined :: Bull)
        monoid' (undefined :: [Int])
        monoid' (undefined :: String)
        monoid' (undefined :: Maybe String)
      it "allows to test applicative laws" $ do
        applicative' (undefined :: [(Int, Int, Int)])
        applicative' (undefined :: Maybe (String, Char, Bool))
      it "allows to test functor laws" $ do
        functor' (undefined :: Two Char (Int, Int, Int))
        functor' (undefined :: Or Char (Int, Int, Int))
      it "Obeys Applicative and Functor laws" $ do
        monoid' (undefined :: A.List Int)
        functor' (undefined :: A.List (Int, Int, Int))
        applicative' (undefined :: A.List (Int, Int, Int))
    describe "ZipList" $ 
      it "obeys laws" $ do
        functor' (undefined :: A.ZipList' (Int, Int, Int))
        applicative' (undefined :: A.ZipList' (Int, Int, Int))
  where
    monoid' x = hspec $ testBatch $ monoid x
    applicative' x = hspec $ testBatch $ applicative x
    functor' x = hspec $ testBatch $ functor x

twoGen :: (Arbitrary a, Arbitrary b) => Gen (Two a b)
twoGen = liftA2 Two arbitrary arbitrary

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = twoGen

orGen :: (Arbitrary a, Arbitrary b) => Gen (Or a b)
orGen = do
  a <- arbitrary
  b <- arbitrary
  oneof [return $ First a, return $ Second b]

instance (Arbitrary a, Arbitrary b) => Arbitrary (Or a b) where
  arbitrary = orGen

data Bull
  = Fools
  | Twoo
  deriving (Eq, Show)

instance (Eq a, Eq b) => EqProp (Two a b) where
  (=-=) = eq

instance (Eq a, Eq b) => EqProp (Or a b) where
  (=-=) = eq

instance Arbitrary Bull where
  arbitrary = frequency [(1, return Fools), (1, return Twoo)]

instance Semigroup Bull where
  Fools <> x = x
  x <> Fools = x
  _ <> _ = Fools

instance Monoid Bull where
  mempty = Fools

instance EqProp Bull where
  (=-=) = eq

instance (Eq a) => EqProp (A.List a) where
  (=-=) = eq

instance (Arbitrary a) => Arbitrary (A.List a) where
  arbitrary = listGen

listGen :: Arbitrary a => Gen (A.List a)
listGen = do
  ar <- arbitrary
  elements [A.Nil, A.Cons ar A.Nil, A.Cons ar (A.Cons ar A.Nil)]

instance (Arbitrary a) => Arbitrary (A.ZipList' a) where
  arbitrary = A.ZipList' <$> listGen

instance Eq a => EqProp (A.ZipList' a) where
  xs =-= ys = xs' `eq` ys'
    where
      xs' =
        let (A.ZipList' l) = xs
         in A.take' 300 l
      ys' =
        let (A.ZipList' l) = ys
         in A.take' 300 l
