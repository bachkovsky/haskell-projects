module HangmanSpec where

import           Hangman
import           Test.Hspec

test :: IO ()
test =
  hspec $ do
    let puzzle = freshPuzzle "word"
        guessed' = puzzle {filled = [Just 'w', Nothing, Nothing, Nothing], guessed = "w"}
        completedPuzzle = Puzzle {word = "FU", filled = [Just 'F', Just 'U'], guessed = "FU", errors = 0}

    describe "fillInCharacter" $ do
      it "correct guess should update 'filled' and 'guessed' lists       " $
        fillInCharacter puzzle 'w' `shouldBe` guessed'
      it "incorrect guess should update 'guessed' list                " $
        fillInCharacter puzzle 'x' `shouldBe` puzzle {guessed = "x"}
      it "incorrect guess on completed puzzle does not affect filled chars list" $
        fillInCharacter completedPuzzle 'X' `shouldBe` completedPuzzle {guessed = "XFU"}

    describe "handleGuess" $ do
      it "guessing already filled character should not change anything" $
        handleGuess guessed' 'w' >>= (`shouldBe` guessed')
      it "incorrect guess should increment error counter" $
        handleGuess puzzle 'x' >>= (`shouldBe` puzzle {errors = 1, guessed = "x"})
