module TraversableSpec where

import Test.Hspec
import Test.Hspec.Checkers
import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes
import Control.Applicative
import Traversable

test :: IO ()
test =
  hspec $ do
    describe "Identity type" $ do
      let identity = undefined :: Identity (Int, Int, [Int])
      it "Obeys functor laws" $ hspec $ testBatch $ functor identity
      it "Obeys traversable laws" $ hspec $ testBatch $ traversable identity
    describe "List type" $ do
      let list = undefined :: List (Int, Int, [Int])
      it "Obeys functor laws" $ hspec $ testBatch $ functor list
      it "Obeys traversable laws" $ hspec $ testBatch $ traversable list
    describe "S type" $ do
      let s = undefined :: (S [] (Int, Int, [Int]))
      it "Obeys functor laws" $ hspec $ testBatch $ functor s
      it "Obeys traversable laws" $ hspec $ testBatch $ traversable s
    describe "Tree' type" $ do
      let tree = undefined :: Tree' (Int, Int, [Int])
      it "Obeys functor laws" $ hspec $ testBatch $ functor tree
      it "Obeys traversable laws" $ hspec $ testBatch $ traversable tree

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = Id <$> arbitrary

instance (Eq a) => EqProp (Identity a) where
  (=-=) = eq

instance (Arbitrary a) => Arbitrary (List a) where
  arbitrary = listGen

listGen :: Arbitrary a => Gen (List a)
listGen = do
  ar <- arbitrary
  elements [Nils, Cons ar Nils, Cons ar (Cons ar Nils)]

instance (Eq a) => EqProp (List a) where
  (=-=) = eq

instance (Arbitrary (n a), Arbitrary a) => Arbitrary (S n a) where
  arbitrary = liftA2 S arbitrary arbitrary

instance (Eq (n a), Eq a) => EqProp (S n a) where
  (=-=) = eq

instance Arbitrary a => Arbitrary (Tree' a) where
  arbitrary = do 
    a <- arbitrary
    a1 <- arbitrary
    a2 <- arbitrary
    a3 <- arbitrary
    a4 <- arbitrary
    a5 <- arbitrary
    elements [Empty, Leaf a, Node (Leaf a1) a2 (Node (Node Empty a4 (Leaf a3)) a5 Empty)]

instance (Eq a) => EqProp (Tree' a) where
  (=-=) = eq