module ArithmeticsSpec where

import Arithmetics
import Test.Hspec
import Test.QuickCheck

test :: IO ()
test =
  hspec $ do
    describe "Addition" $ do
      it "1 add 1 is greater than 1" $ (1 `add` 1) > 1 `shouldBe` True
      it "2 add 2 is equal to 4" $ (2 `add` 2) `shouldBe` 4
    describe "Division" $ do
      it "15 divided by 3 is 5" $ (15 `dividedBy` 3) `shouldBe` (5, 0)
      it "22 divided by 5 is 4 rem. 2" $ (22 `dividedBy` 5) `shouldBe` (4, 2)
    describe "Multiplication" $ do
      it "1 multiplied by 0 is 0" $ (1 `multiply` 0) `shouldBe` 0
      it "2 multiplied by 0 is 0" $ (2 `multiply` 0) `shouldBe` 0
      it "0 multiplied by 1 is 0" $ (0 `multiply` 1) `shouldBe` 0
      it "0 multiplied by 2 is 0" $ (0 `multiply` 2) `shouldBe` 0
      it "1 multiplied by 1 is 1" $ (1 `multiply` 1) `shouldBe` 1
      it "3 multiplied by 1 is 3" $ (3 `multiply` 1) `shouldBe` 3
      it "1 multiplied by 3 is 3" $ (1 `multiply` 3) `shouldBe` 3
      it "5 multiplied by 4 is 20" $ (5 `multiply` 4) `shouldBe` 20
    describe "Comparison" $ do
      it "x + 1 is always greater than x" $ property $ \x -> (x + 1) `isGreater` (x :: Int)
      it "x - 1 is always smaller than x" $ property $ \x -> (x - 1) `isSmaller` (x :: Int)

trivialInt :: Gen Int
trivialInt = return 1

oneThroughThree :: Gen Int
oneThroughThree = elements [1, 2, 3]

genBool :: Gen Bool
genBool = choose (False, True)

genBool' :: Gen Bool
genBool' = elements [False, True]

genOrdering :: Gen Ordering
genOrdering = elements [LT, EQ, GT]

genChar :: Gen Char
genChar = elements ['a' .. 'z']

genTuple :: (Arbitrary a, Arbitrary b) => Gen (a, b)
genTuple = do
  a <- arbitrary
  b <- arbitrary
  return (a, b)

genEither :: (Arbitrary a, Arbitrary b) => Gen (Either a b)
genEither = do
  a <- arbitrary
  b <- arbitrary
  elements [Left a, Right b]

-- equal probability
genMaybe :: Arbitrary a => Gen (Maybe a)
genMaybe = do
  a <- arbitrary
  elements [Nothing, Just a]

-- What QuickCheck actually does
-- so you get more Just values
genMaybe' :: Arbitrary a => Gen (Maybe a)
genMaybe' = do
  a <- arbitrary
  frequency [(1, return Nothing), (3, return (Just a))]