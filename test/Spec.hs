import qualified ArithmeticsSpec as AS
import qualified HangmanSpec as HS
import qualified FunctorSpec as FS
import qualified TraversableSpec as TS
import qualified IniSpec as IS
import qualified QueueSpec as QS
import Test.QuickCheck
import Test.QuickCheck.Gen (oneof)

main :: IO ()
main = do
  AS.test
  HS.test
  FS.test
  TS.test
  IS.main
  QS.test

data Trivial =
  Trivial
  deriving (Eq, Show)

trivialGen :: Gen Trivial
trivialGen = return Trivial

instance Arbitrary Trivial where
  arbitrary = trivialGen

newtype Identity a =
  Identity a
  deriving (Eq, Show)

identityGen :: Arbitrary a => Gen (Identity a)
identityGen = Identity <$> arbitrary

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = identityGen

identityGenInt :: Gen (Identity Int)
identityGenInt = identityGen

data Sum a b
  = First a
  | Second b
  deriving (Eq, Show)

sumGenEqual :: (Arbitrary a, Arbitrary b) => Gen (Sum a b)
sumGenEqual = do
  a <- arbitrary
  b <- arbitrary
  oneof [return $ First a, return $ Second b]