module Main where

import Criterion.Main
import qualified Queue as Q

infixl 9 !?

(!?) :: [a] -> Int -> Maybe a
{-# INLINABLE (!?) #-}
xs !? n
  | n < 0 = Nothing
  | otherwise =
    foldr
      (\x r k ->
         case k of
           0 -> Just x
           _ -> r (k - 1))
      (const Nothing)
      xs
      n

myList :: [Int]
myList = [1 .. 9999]

newtype DList a = DL
  { unDL :: [a] -> [a]
  }

empty :: DList a
empty = DL id

{-# INLINE empty #-}
singleton :: a -> DList a
singleton = DL . (:)

{-# INLINE singleton #-}
toList :: DList a -> [a]
toList (DL f) = f []

-- Prepend a single element to a dlist.
infixr `cons`

cons :: a -> DList a -> DList a
cons x xs = DL ((x :) . unDL xs)

{-# INLINE cons #-}
 -- Append a single element to a dlist.
infixl `snoc`

snoc :: DList a -> a -> DList a
snoc xs x = DL (unDL xs . (x :))

{-# INLINE snoc #-}
-- Append dlists.
append :: DList a -> DList a -> DList a
append xd yd = DL (unDL xd . unDL yd)

schlemiel :: Int -> [Int]
schlemiel i = go i []
  where
    go 0 xs = xs
    go n xs = go (n - 1) (n : xs)

constructDlist :: Int -> [Int]
constructDlist i = toList $ go i empty
  where
    go 0 xs = xs
    go n xs = go (n - 1) (singleton n `append` xs)

popAll :: (Eq (q Int), Q.IQueue q) => q Int -> [Int]
popAll = go [] 
  where
  go xs q = case Q.pop q of
    Nothing -> xs
    Just (e, q')
      | Q.isEmpty q' -> e:xs
      | otherwise -> go (e:xs) q'

pushAll :: Q.IQueue q => Int -> q Int
pushAll n = foldr Q.push Q.empty [1..n]

benchQueue :: Int -> [Int]
benchQueue n = let q = pushAll n in popAll (q::Q.Queue Int)
benchSQueue :: Int -> [Int]
benchSQueue n = let q = pushAll n in popAll (q::Q.SQueue Int)

{-# INLINE append #-}
main :: IO ()
main =
  defaultMain
    [ 
    --   bench "index list 9999" $ whnf (myList !!) 9998
    -- , bench "index list maybe 9999" $ whnf (myList !?) 9998
    -- , bench "map list 9999" $ nf (map (+ 1)) myList
    -- , bench "concat list" $ nf schlemiel 123456
    -- , bench "concat dlist" $ nf constructDlist 123456
    bench "traverse queue" $ whnf benchQueue 5000
    , bench "traverse single list queue" $ whnf benchSQueue 5000
    ]
